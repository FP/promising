# The promising machine without promises

This repository contains a variant of the [promising semantics][promising].

The main difference with the original Coq development is that this variant is
developed in a direct style that is compatible with Iris. More specifically,
we do not use Coq modules and instead use type classes and finite
structures from (and in the style) [Std++][stdpp].

Additionally, the development is in a
state such that only the machine without promises is the main concern.
The support for promises is available, but its correctness is not guaranteed.


## More differences

### Event-labelled semantics
The machine label its execution with events. It is up to the language to
bind the events to the execution of language expressions. This is done in the
style of [iGPS][iGPS].

### Allocation and deallocation
All the components of the machine state are modeled with finite structures.
The most notable are the views, which are now just finite partial maps, instead
of total functions. Their stark difference emerges in allocation and deallocation:
if a location is not defined in a thread's view, it means
that the view has not observed the allocation event for that location.
The allocation (deallocation) event is marked with a specifical value `AVal`
(rsp. `DVal`).

A complication arises for release views. In the original machine, the release
view is always defined for all locations. In the current machine, it is not the
case. So for all locations that are not available (undefined) in the current
thread's view, they get a default release view, which is the thread's view from
the last release fence. This is stored as the component `frel` of `tview`.

Writing `AVal`, reading and writing `DVal` are all undefined behaviors (UB).
These values can only be written by primitive allocation and deallocation events.

### Poisons and uninitialized reads
To allow poison values in style of LLVM,
we allow reads to read the allocation value `AVal` from the machine. It is then
up to the language to decide to turn this value into UB or a poison value.
It is also up to the language to add a poison value.

The machine takes the set of "storable" values as parameter from the language.
It is up to the language to add poisons to this set.
If the language does so, then the machine allows reading and writing poisons.

### Non-atomics

In the original promising semantics, there is no UB for racing between `plain`
accesses. In the current semantics, we treat racing between `plain`
accesses as UB, so as to model non-atomics. We implement a race detector similar
to the one in [iGPS][iGPS].

#### TODO: the race detector is unsound!

Note that there's a difference in the race detector between non-atomic reads
and other operations.

We also do not have any guarantees about `plain` promises.

### Non-deterministic comparisons

The machine does not case about equalities of values. It is up to the language to
add comparison semantics, which can be non-deterministic. Note that non-determinism
can interfere with the DRF (data-race-freedom) theorem.
This is because different race-checking conditions are applied when CASes
fail non-deteministically due to non-deterministic comparison. To be sure,
the language should forbid CAS-ing with non-atomics (i.e. `plain` accesses),
which then will ensure that the same race-checking conditions are always applied.

An additional consequence is that the machine itself does not provide DRF.

### Monotonicity

The current development also prove that a thread behavior is monotone with
respect to the thread's view. That is if `V1 ⊑ V2`, every behavior of the thread
starting with thread-view `V2` is also possible if the thread starts at `V1`.
This proof also requires a monotone condition on the machine states.

This proof only applies to the machine without promises.

## Dependencies

The master branch is known to compile with:
* Coq 8.9.1 / 8.10.1
* A development version of [Std++][stdpp] (see [opam](opam) for the exact version)

## Build

The recommended way to build the development is using OPAM (version 1.2.2).

To find all the dependencies of ra-gps, opam needs to know about the Coq and Iris
opam archives. This can be achieved by executing
```
opam repo add coq-released https://coq.inria.fr/opam/released
opam repo add iris-dev https://gitlab.mpi-sws.org/iris/opam.git
```

Now, execute `make build-dep` to install all dependencies.
Finally, execute `make` to build the development.



[promising]: http://sf.snu.ac.kr/promise-concurrency/
[iGPS]: https://gitlab.mpi-sws.org/FP/sra-gps
[stdpp]: https://gitlab.mpi-sws.org/iris/stdpp
