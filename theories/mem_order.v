From promising Require Export base.

Inductive memOrder := | Plain | Relaxed | StrongRelaxed | AcqRel | SeqCst .

Definition memOrder_le : relation memOrder :=
  λ o1 o2,
    match o1, o2 with
    | Plain, _ => True
    | _, Plain => False

    | Relaxed, _ => True
    | _, Relaxed => False

    | StrongRelaxed, _ => True
    | _, StrongRelaxed => False

    | AcqRel, _ => True
    | _, AcqRel => False

    | SeqCst, SeqCst => True
    end.

Instance memOrder_dec : EqDecision memOrder.
Proof. solve_decision. Defined.

Instance memOrder_countable : Countable memOrder.
Proof.
  refine(inj_countable'
    (λ v, match v with
          | Plain => 0 | Relaxed => 1 | StrongRelaxed => 2 | AcqRel => 3 | SeqCst => 4
          end)
    (λ x, match x with
          | 0 => Plain | 1 => Relaxed | 2 => StrongRelaxed | 3 => AcqRel | _ => SeqCst
          end) _); by intros [].
Qed.


Instance memOrder_le_dec : RelDecision memOrder_le.
Proof. move => [] []; firstorder. Defined.

Program Canonical Structure memOrder_Lat :=
  Make_Lat memOrder (=) memOrder_le
     (λ o1 o2, if (decide (memOrder_le o1 o2)) then o2 else o1)
     (λ o1 o2, if (decide (memOrder_le o1 o2)) then o1 else o2)
     _ _ _ _ _ _ _ _ _ _ _ _ _.
Next Obligation. repeat constructor. Qed.
Next Obligation. split; [move=>[]//|move=>[][]//[]//]. Qed.
Next Obligation. move=>[][]//. Qed.
Next Obligation. move=>[][]//. Qed.
Next Obligation. move=>[][]//. Qed.
Next Obligation. move=>[][][]//. Qed.
Next Obligation. move=>[][]//. Qed.
Next Obligation. move=>[][]//. Qed.
Next Obligation. move=>[][][]//. Qed.
