From promising Require Export thread.
Set Default Proof Using "Type".

Section Monotone.
  Context `{LocFacts loc} `{CVAL: Countable VAL} `{Shift loc} `{!Allocator}.

  Notation memory := (@memory _ _ VAL).
  Notation message := (@message _ _ VAL).
  Notation event := (@event loc VAL).
  Notation baseMessage := (@baseMessage loc _ VAL).
  Notation program_step := (@program_step _ _ VAL _ _).
  Implicit Types (M: memory) (𝑚: message) (𝓥: threadView) (𝑚s : list message) (m: baseMessage).

  Lemma vmono_plain_write_helper l t 𝓥1 𝓥2 𝓥2' (Ext: 𝓥1 ⊑ 𝓥2) :
    write_helper 𝓥2 Plain l t ∅ None 𝓥2' →
    ∃ 𝓥1', 𝓥1' ⊑ 𝓥2' ∧ write_helper 𝓥1 Plain l t ∅ None 𝓥1'.
  Proof.
    intros WH. inversion WH; clear WH; subst; simpl in *.
    eexists.
    split; [|econstructor].
    - constructor=>/=; [|by rewrite Ext..]. move=>l'.
      case (decide (l' = l)) => [->|?];
        [rewrite 2!lookup_insert
        |do 2 (rewrite lookup_insert_ne; last auto); by apply Ext].
      do 2 f_equiv. have ?: rel 𝓥1 !! l ⊑ rel 𝓥2 !! l by apply Ext.
      destruct (rel 𝓥1 !! l), (rel 𝓥2 !! l)=>//; solve_lat.
    - eapply strict_transitive_r; [by apply Ext|done].
  Qed.

  Lemma vmono_alloc_helper 𝑚s 𝓥1 𝓥2 𝓥2' (Ext: 𝓥1 ⊑ 𝓥2) :
    alloc_helper 𝑚s 𝓥2 𝓥2' → ∃ 𝓥1', 𝓥1' ⊑ 𝓥2' ∧ alloc_helper 𝑚s 𝓥1 𝓥1'.
  Proof.
    intros ALLOC. induction ALLOC as [𝓥2|? ? 𝓥2 𝓥3 𝓥4].
    - eexists. split; [|constructor]. done.
    - destruct (IHALLOC Ext) as [𝓥1' [Ext1' HE]].
      destruct (vmono_plain_write_helper _ _ _ _ _ Ext1' WRITE) as [?[LE WH]].
      eexists. split.
      + exact LE.
      + econstructor. exact HE. exact WH.
  Qed.

  Lemma vmono_alloc_preservation l n σ1 σ2 𝓥1 𝓥2 𝓥2' (Ext: 𝓥1 ⊑ 𝓥2) :
    let cfg 𝓥 σ:= (mkCFG (mkLC 𝓥 ∅) σ) in
    program_step true (cfg 𝓥2 σ1) (Alloc l n) (cfg 𝓥2' σ2) →
    ∃ 𝓥1', 𝓥1' ⊑ 𝓥2' ∧
      program_step true (cfg 𝓥1 σ1) (Alloc l n) (cfg 𝓥1' σ2).
  Proof.
    intros ? STEP.
    inversion STEP; clear STEP; subst; simpl in *.
    inversion ALLOC; clear ALLOC; subst.
    destruct (vmono_alloc_helper _ _ _ _ Ext VALL) as [?[Ext1' AH]].
    eexists. split.
    exact Ext1'. by do 2 constructor.
  Qed.

  Lemma vmono_dealloc_preservation l n σ1 σ2 𝓥1 𝓥2 𝓥2' (Ext: 𝓥1 ⊑ 𝓥2) :
    let cfg 𝓥 σ:= (mkCFG (mkLC 𝓥 ∅) σ) in
    program_step true (cfg 𝓥2 σ1) (Dealloc l n) (cfg 𝓥2' σ2) →
    (∃ c2', program_step true (cfg 𝓥1 σ1) (Dealloc l n) c2') →
    ∃ 𝓥1', 𝓥1' ⊑ 𝓥2' ∧
      program_step true (cfg 𝓥1 σ1) (Dealloc l n) (cfg 𝓥1' σ2).
  Proof.
    intros ? STEP STEP'.
    inversion STEP; clear STEP; subst; simpl in *.
    inversion DEALLOC; clear DEALLOC; subst.
    destruct (vmono_alloc_helper _ _ _ _ Ext VALL) as [?[Ext1' AH]].
    eexists. split.
    exact Ext1'. constructor. by constructor.
    constructor.
    - simpl. destruct STEP' as [c2' STEP'].
      inversion STEP'; clear STEP'; subst; simpl in *.
      inversion DEALLOC; clear DEALLOC; subst.
      move => 𝑚 /elem_of_list_lookup [n' In'].
      have EqL := memory_dealloc_loc_eq _ _ _ _ _ MEMALL _ _ In'.
      have LEN : (n' < Pos.to_nat n)%nat.
      { rewrite -(memory_dealloc_length _ _ _ _ _ MEMALL).
        apply lookup_lt_is_Some_1. by eexists. }
      rewrite -(memory_dealloc_length _ _ _ _ _ MEMALL0) in LEN.
      apply lookup_lt_is_Some in LEN as [𝑚' In3].
      have EqL' := memory_dealloc_loc_eq _ _ _ _ _ MEMALL0 _ _ In3.
      rewrite EqL -EqL'.
      inversion DRF0. apply LOCAL. by eapply elem_of_list_lookup_2.
    - by inversion DRF.
  Qed.

  Lemma vmono_read_helper l o t R 𝓥1 𝓥2 𝓥2' (Ext: 𝓥1 ⊑ 𝓥2):
    read_helper 𝓥2 o l t R 𝓥2' → ∃ 𝓥1', 𝓥1' ⊑ 𝓥2' ∧ read_helper 𝓥1 o l t R 𝓥1'.
  Proof.
    intros RH. inversion RH; clear RH; subst.
    eexists. split; last econstructor.
    - constructor; simpl; [apply Ext|apply Ext|..].
      + subst cur'. case (decide (AcqRel ⊑ _)) => ?; rewrite Ext; solve_lat.
      + subst acq'. case_match; rewrite Ext; solve_lat.
    - etrans; [apply Ext|apply PLN].
    - done.
    - move => /RLX RLX2. etrans; [apply Ext|apply RLX2].
  Qed.

  Lemma vmono_read_preservation l v o σ1 σ2 𝓥1 𝓥2 𝓥2' (Ext: 𝓥1 ⊑ 𝓥2) :
    let cfg 𝓥 σ:= (mkCFG (mkLC 𝓥 ∅) σ) in
    program_step true (cfg 𝓥2 σ1) (Read l v o) (cfg 𝓥2' σ2) →
    (∃ v' c2', program_step true (cfg 𝓥1 σ1) (Read l v' o) c2') →
    ∃ 𝓥1', 𝓥1' ⊑ 𝓥2' ∧ program_step true (cfg 𝓥1 σ1) (Read l v o) (cfg 𝓥1' σ2).
  Proof.
    intros ? STEP STEP'.
    inversion STEP; clear STEP; subst; simpl in *.
    inversion READ; clear READ; subst.
    destruct (vmono_read_helper _ _ _ _ _ _ _ Ext READ0) as [𝓥1' [? RH]].
    exists 𝓥1'. split; [done|]. constructor; [done|..].
    destruct STEP' as [v' [c2' STEP']].
    inversion STEP'; clear STEP'; subst. inversion DRF0; clear DRF0.
    rewrite H4 in LOCAL, MAX. by econstructor.
  Qed.

  Lemma vmono_memory_lookup_sqsubseteq 𝑚 M1 M2 V P1 P2
    (Ext: V ⊑ 𝑚.(mbase).(mrel)):
    memory_write P1 M1 𝑚 P2 M2 OpKindAddIns true →
    let 𝑚' := (mkMsg 𝑚.(mloc) 𝑚.(mto) (mkBMes 𝑚.(mbase).(mval) 𝑚.(mbase).(mfrom) V)) in
    let M2' := <[mloc 𝑚':=
                  <[mto 𝑚':=(mkBMes 𝑚'.(mbase).(mval) 𝑚'.(mbase).(mfrom) V)]>
                          (M1 !!c 𝑚.(mloc))]> M1 in
    memory_le M2' M2.
  Proof.
    move => WR 𝑚' M2' l.
    have EqM2 := memory_write_addins_eq _ _ _ _ _ _ WR.
    rewrite EqM2 /M2'.
    case (decide (l = 𝑚.(mloc))) => [->|?].
    - rewrite memory_uncurry_lookup_insert; last apply insert_non_empty.
      rewrite memory_uncurry_lookup_insert; last apply insert_non_empty.
      constructor => t.
      case (decide (t = 𝑚.(mto))) => [->|?];
        [by rewrite 2!lookup_insert; constructor
        |by do 2 (rewrite lookup_insert_ne; last done)].
    - rewrite memory_uncurry_lookup_insert_ne;
        [|done|by apply insert_non_empty].
      rewrite memory_uncurry_lookup_insert_ne;
        [done|done|by apply insert_non_empty].
  Qed.

  Lemma vmono_memory_write_1 𝑚 M1 M2 V P1 P2
    (Ext: V ⊑ 𝑚.(mbase).(mrel))
    (INCL: ∀ V', V = Some V' →
          V'.(pln) !! 𝑚.(mloc) ⊑ Some (𝑚.(mto)) ∧ Some (𝑚.(mto)) ⊑ V'.(rlx) !! 𝑚.(mloc)):
    memory_write P1 M1 𝑚 P2 M2 OpKindAddIns true →
    let 𝑚' := (mkMsg 𝑚.(mloc) 𝑚.(mto)
                     (mkBMes 𝑚.(mbase).(mval) 𝑚.(mbase).(mfrom) V)) in
    let M2' := <[mloc 𝑚':=
                  <[mto 𝑚':=(mkBMes 𝑚'.(mbase).(mval) 𝑚'.(mbase).(mfrom) V)]>
                          (M1 !!c 𝑚.(mloc))]> M1 in
    V ∈ M2' →
    memory_write P1 M1 𝑚' P2 M2' OpKindAddIns true.
  Proof.
    move => WR 𝑚' M2' In'.
    inversion_clear WR.
    apply (MemWrite _ _ _ _ _ _ _
              (<[𝑚'.(mloc) := (<[𝑚'.(mto) := 𝑚'.(mbase) ]>(P1 !!c 𝑚'.(mloc)))]>P1)).
    - inversion_clear PROMISE.
      constructor; [..|done|done|done|done].
      + do 2 constructor. inversion_clear PROMISE0. by inversion ADD.
      + do 2 constructor. inversion_clear MEM. by inversion ADD.
      + constructor; [apply mWF|]. move => ? /=. by apply INCL.
    - inversion_clear REMOVE.
      inversion PROMISE. inversion PROMISE0. subst.
      inversion_clear REMOVE0.
      inversion ADD.
      rewrite memory_cell_insert_insert memory_cell_lookup_insert.
      rewrite delete_insert_delete.
      rewrite -(memory_cell_insert_insert 𝑚.(mloc) (delete _ _)
                                        (<[mto 𝑚:=mbase 𝑚']> (P1 !!c mloc 𝑚))).
      rewrite (_: 𝑚.(mloc) = 𝑚'.(mloc)); last done.
      rewrite (_: 𝑚.(mto) = 𝑚'.(mto)); last done.
      constructor.
      rewrite memory_cell_lookup_insert -(delete_insert_delete _ _ 𝑚'.(mbase)).
      constructor. by rewrite lookup_insert.
  Qed.

  Lemma vmono_write_helper l o t v tf (Vr: view) 𝓥1 𝓥2 𝓥2' V2 M
    (Ext: 𝓥1 ⊑ 𝓥2) (In: 𝓥1 ∈ M) (Inr: Vr ∈ M) (FRESH: M !! (l,t) = None)
    (LEr: Vr.(pln) !! l ⊑ Some t) :
    write_helper 𝓥2 o l t Vr V2 𝓥2' →
    ∃ 𝓥1' V1, 𝓥1' ⊑ 𝓥2' ∧ V1 ⊑ V2
      ∧ (∀ V', V1 = Some V' → V'.(pln) !! l ⊑ Some t ∧ Some t ⊑ V'.(rlx) !! l)
      ∧ (let M' := <[l:=<[t:=(mkBMes v tf V1)]>(M !!c l)]> M in V1 ∈ M')
      ∧ write_helper 𝓥1 o l t Vr V1 𝓥1'.
  Proof.
    intro WH. inversion WH; clear WH; simpl in *.
    have ExtR: rel 𝓥1 !! l ⊑ rel 𝓥2 !! l by apply Ext.
    have ExtRO: from_option (A:=view) id ∅ (rel 𝓥1 !! l) ⊑ from_option id ∅ (rel 𝓥2 !! l)
      by destruct (rel 𝓥1 !! l), (rel 𝓥2 !! l).
    have Inr': ∀ m, Vr ∈ <[l:=<[t:=m]> (M !!c l)]> M.
    { move => m. apply closed_timemap_wf_view.
      move => l1 t1.
      case (decide (l1 = l)) => [->|?] Eqt1.
      - setoid_rewrite lookup_mem_first_eq.
        apply Inr in Eqt1 as [m2 [t2 [Le2 Eqt2]]].
        exists m2. exists t2. split; first done.
        rewrite lookup_insert_ne; first by rewrite -memory_lookup_cell.
        move => ?. subst t2. by rewrite FRESH in Eqt2.
      - apply Inr in Eqt1 as [m2 [t2 [Le2 Eqt2]]].
        exists m2, t2. by rewrite (lookup_mem_first_ne l l1) //. }
    do 2 eexists.
    split; [|split; [|split; [|split; [|constructor]]]].
    - split=>/=; [|by rewrite Ext..].
      subst => l'.
      case (decide (l' = l)) => [->|?];
        [rewrite 2!lookup_insert
        |do 2 (rewrite lookup_insert_ne //); by apply Ext].
      case_match; rewrite ExtRO ?Ext //.
    - unfold write_Rw. case_match; [case_match|]; by rewrite ?ExtRO ?Ext.
    - move => V0. rewrite /write_Rw. case (decide _); intros ? [= <-]. split.
      + do 3 rewrite lookup_join_pln lookup_join.
        apply lat_join_lub; [apply lat_join_lub|done]; [apply lat_join_lub|..].
        * rewrite ExtRO rel_cur' pln_rlx. by apply strict_include.
        * case_match; simpl; [rewrite lookup_join|]; rewrite lookup_insert; [|done].
          apply lat_join_lub; [|done]. rewrite Ext pln_rlx. by apply strict_include.
        * rewrite frel_cur Ext pln_rlx. by apply strict_include.
      + case_match; rewrite /= !lookup_join lookup_singleton; solve_lat.
    - unfold write_Rw. case_match=>//. repeat apply join_closed_view=>//.
      + apply closed_view_memory_fresh_insert_mono=>//.
        pose proof (closed_tview_rel _ _ In l). by destruct (rel 𝓥1 !! l).
      + have InV: ∀ m, mkView_same {[l := t]} ∈ <[l:=<[t:=m]> (M !!c l)]> M.
        { move => ?. apply closed_timemap_wf_view=>/= l' t'.
          case (decide (l' = l)) => [->|?];
            [rewrite lookup_insert|by rewrite lookup_insert_ne].
          move => [<-]. eexists. exists t. split; first done.
          by rewrite memory_lookup_cell memory_cell_lookup_insert
                     lookup_insert. }
        case_match; [apply join_closed_view|]; [|apply InV..].
        apply closed_view_memory_fresh_insert_mono=>//. apply In.
      + apply closed_view_memory_fresh_insert_mono=>//. apply In.
    - eapply strict_transitive_r; [by apply Ext|by apply RLX].
  Qed.

  Lemma vmono_write_preservation l v o σ1 σ2 𝓥1 𝓥2 𝓥2'
    (Ext: 𝓥1 ⊑ 𝓥2) (In1: 𝓥1 ∈ σ1.(mem)) (WF: Wf σ1):
    let cfg 𝓥 σ:= (mkCFG (mkLC 𝓥 ∅) σ) in
    program_step true (cfg 𝓥2 σ1) (Write l v o) (cfg 𝓥2' σ2) →
    (∃ c2', program_step true (cfg 𝓥1 σ1) (Write l v o) c2') →
    ∃ 𝓥1' σ1', 𝓥1' ⊑ 𝓥2' ∧ σ1' ⊑ σ2 ∧
      program_step true (cfg 𝓥1 σ1) (Write l v o) (cfg 𝓥1' σ1').
  Proof.
    intros ? STEP RED.
    inversion STEP; clear STEP; subst; simpl in *.
    inversion WRITE; clear WRITE; subst.
    destruct (vmono_write_helper 𝑚.(mloc) o 𝑚.(mto) 𝑚.(mbase).(mval)
                                 𝑚.(mbase).(mfrom) ∅ _ _ 𝓥2' 𝑚.(mbase).(mrel) _
                                 Ext In1) as
      [𝓥1' [V1 [Ext1' [Ext2' [WFV1 [InV1 WH']]]]]]; [done|..|done|done|].
    - eapply memory_write_addins_fresh. by eauto. by apply WF.
    - exists 𝓥1'.
      have ?: k = OpKindAddIns.
      { inversion WRITE0. inversion PROMISE. by inversion PROMISE0. }
      subst k.
      set M' :=
         <[mloc 𝑚:=<[mto 𝑚:={|
                            mval := mval (mbase 𝑚);
                            mfrom := mfrom (mbase 𝑚);
                            mrel := V1 |}]> (mem σ1 !!c mloc 𝑚)]>
           (mem σ1).
      have WR := vmono_memory_write_1 _ _ _ _ _ _ Ext2' WFV1 WRITE0 InV1.
      simpl in WR.
      set 𝑚' := mkMsg 𝑚.(mloc) 𝑚.(mto)
                      (mkBMes 𝑚.(mbase).(mval) 𝑚.(mbase).(mfrom) V1).
      exists (mkGB σ1.(sc) 𝓝2 M'). split; first done. split.
      + constructor=>//=. by eapply vmono_memory_lookup_sqsubseteq.
      + rewrite (_: 𝑚.(mloc) = 𝑚'.(mloc)); last done.
        constructor; [done|by econstructor|].
        simpl in *. destruct RED as [c2' RED].
        inversion RED;clear RED; subst. simpl in DRF0.
        inversion DRF0; subst.
        inversion DRF; subst. econstructor; last done.
        inversion LOCAL.
        rewrite H3 in OTL, LAST.
        by constructor.
  Qed.

   Lemma vmono_update_preservation l vr vw or ow σ1 σ2 𝓥1 𝓥2 𝓥2'
    (Ext: 𝓥1 ⊑ 𝓥2) (In1: 𝓥1 ∈ σ1.(mem)) (WF: Wf σ1):
    let cfg 𝓥 σ:= (mkCFG (mkLC 𝓥 ∅) σ) in
    program_step true (cfg 𝓥2 σ1) (Update l vr vw or ow) (cfg 𝓥2' σ2) →
    (∃ v2' c2', program_step true (cfg 𝓥1 σ1) (Read l v2' or) c2') →
    ∃ 𝓥1' σ1', 𝓥1' ⊑ 𝓥2' ∧ σ1' ⊑ σ2 ∧
      program_step true (cfg 𝓥1 σ1) (Update l vr vw or ow) (cfg 𝓥1' σ1').
  Proof.
    intros ? STEP RED.
    inversion STEP; clear STEP; subst; simpl in *.
    inversion READ; clear READ; subst.
    inversion WRITE; clear WRITE; subst.
    destruct (vmono_read_helper _ _ _ _ _ _ _ Ext READ0) as [𝓥' [Ext2 RH]].
    have WFm: Wf 𝑚1. { eapply msg_elem_wf_pre. eauto. apply WF. }
    have In': 𝓥' ∈ σ1.(mem).
    { apply (read_helper_closed_tview _ _ _ _ _ _ _ RH In1); last by eexists.
      have ? := mem_wf_closed _ (global_wf_mem _ WF) _ _ _ IN.
      by destruct (𝑚1.(mbase).(mrel)). }
    destruct (vmono_write_helper 𝑚2.(mloc) ow 𝑚2.(mto) 𝑚2.(mbase).(mval)
                                 𝑚2.(mbase).(mfrom)
                                 (from_option id ∅ 𝑚1.(mbase).(mrel))
                                 _ _ 𝓥2' 𝑚2.(mbase).(mrel) _
                                 Ext2 In') as
      [𝓥1' [V1 [Ext1' [Ext2' [WFV1 [InV1 WH']]]]]]; [..|done|].
    - have INRL := mem_wf_closed _ (global_wf_mem _ WF) _ _ _ IN.
      by destruct 𝑚1.(mbase).(mrel).
    - eapply memory_write_addins_fresh. by eauto. by apply WF.
    - rewrite -SAME. etrans; first (inversion RH; apply PLN2).
      rewrite ADJ. change (𝑚2.(mbase).(mfrom) ⊑ 𝑚2.(mto)).
      apply Qcanon.Qclt_le_weak, msg_wfr_interval.
      inversion WRITE0. by inversion PROMISE.
    - exists 𝓥1'.
      have ?: k = OpKindAddIns.
      { inversion WRITE0. inversion PROMISE. by inversion PROMISE0. }
      subst k.
      set M' :=
         <[mloc 𝑚2:=<[mto 𝑚2:={|
                            mval := mval (mbase 𝑚2);
                            mfrom := mfrom (mbase 𝑚2);
                            mrel := V1 |}]> (mem σ1 !!c mloc 𝑚2)]>
           (mem σ1).
      have WR := vmono_memory_write_1 _ _ _ _ _ _ Ext2' WFV1 WRITE0 InV1.
      simpl in WR.
      set 𝑚' := mkMsg 𝑚2.(mloc) 𝑚2.(mto)
                      (mkBMes 𝑚2.(mbase).(mval) 𝑚2.(mbase).(mfrom) V1).
      exists (mkGB σ1.(sc) 𝓝3 M'). split; first done. split.
      + constructor=>//=. by eapply vmono_memory_lookup_sqsubseteq.
      + destruct RED as [v2' [c2' RED]].
        eapply (PStepU _ (cfg 𝓥1 σ1) 𝑚1 𝑚');
          [done|done|done|done|by constructor|by econstructor|..].
        * simpl in *.
          inversion RED; clear RED; subst. simpl in *.
          inversion DRF; clear DRF. rewrite H3 in LOCAL, MAX.
          by econstructor.
        * simpl in *.
          inversion RED; clear RED; subst. simpl in *.
          inversion DRF; subst.
          inversion DRFW; subst. econstructor; last done.
          inversion LOCAL.
          econstructor. eauto. rewrite H3 SAME in OTL, LAST.
          etrans; first apply LAST. rewrite -OTL.
          apply (read_helper_tview_sqsubseteq _ _ _ _ _ _ RH).
  Qed.

  Lemma vmono_acq_fence_preservation σ1 σ2 𝓥1 𝓥2 𝓥2'
    (Ext: 𝓥1 ⊑ 𝓥2) :
    let cfg 𝓥 σ:= (mkCFG (mkLC 𝓥 ∅) σ) in
    program_step true (cfg 𝓥2 σ1) (Fence AcqRel Relaxed) (cfg 𝓥2' σ2) →
    ∃ 𝓥1', 𝓥1' ⊑ 𝓥2' ∧
      program_step true (cfg 𝓥1 σ1) (Fence AcqRel Relaxed) (cfg 𝓥1' σ1).
  Proof.
    intros cfg STEP. inversion STEP; clear STEP; subst; simpl in *.
    inversion FACQ; clear FACQ; subst.
    eexists. split; last by (constructor; constructor). constructor; apply Ext.
  Qed.

  Lemma vmono_rel_fence_preservation σ1 σ2 𝓥1 𝓥2 𝓥2'
    (Ext: 𝓥1 ⊑ 𝓥2) :
    let cfg 𝓥 σ:= (mkCFG (mkLC 𝓥 ∅) σ) in
    program_step true (cfg 𝓥2 σ1) (Fence Relaxed AcqRel) (cfg 𝓥2' σ2) →
    ∃ 𝓥1', 𝓥1' ⊑ 𝓥2' ∧
      program_step true (cfg 𝓥1 σ1) (Fence Relaxed AcqRel) (cfg 𝓥1' σ1).
  Proof.
    intros cfg STEP. inversion STEP; clear STEP; subst; simpl in *.
    inversion FREL; clear FREL; subst.
    eexists. split; last by (constructor; constructor). constructor; try apply Ext.
  Qed.

  Lemma vmono_sc_fence_preservation σ1 σ2 𝓥1 𝓥2 𝓥2'
    (Ext: 𝓥1 ⊑ 𝓥2) :
    let cfg 𝓥 σ:= (mkCFG (mkLC 𝓥 ∅) σ) in
    program_step true (cfg 𝓥2 σ1) (Fence SeqCst SeqCst) (cfg 𝓥2' σ2) →
    ∃ 𝓥1' σ1', 𝓥1' ⊑ 𝓥2' ∧ σ1' ⊑ σ2 ∧
      program_step true (cfg 𝓥1 σ1) (Fence SeqCst SeqCst) (cfg 𝓥1' σ1').
  Proof.
    intros cfg STEP. inversion STEP; clear STEP; subst; simpl in *.
    inversion FSC; clear FSC; subst.
    inversion SC; clear SC; subst; simpl in *.
    do 2 eexists.
    split; last split; last by do 2 constructor; simpl.
    - constructor.
      + apply Ext.
      + constructor; (apply lat_join_mono; [by apply Ext|done]).
      + constructor; (apply lat_join_mono; [by apply Ext|done]).
      + constructor; (apply lat_join_mono; [by apply Ext|done]).
    - constructor; [|done|done]. by rewrite /= Ext.
  Qed.
End Monotone.
