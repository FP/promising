Require Import Coq.QArith.Qcanon.
From stdpp Require Export gmap.

From promising Require Export thread.
Set Default Proof Using "Type".

Section Wellformedness.
   Context `{LocFacts loc} `{CVAL: Countable VAL} `{Shift loc}
           `{!Allocator (state:=@memory loc _ VAL)}.

  Notation memory := (@memory _ _ VAL).
  Notation message := (@message _ _ VAL).
  Notation event := (@event loc VAL).
  Notation local := (@local loc _ VAL).
  Notation global := (@global loc _ VAL).
  Notation config := (@config loc _ VAL).

  Implicit Type (𝑚: message) (M: memory) (𝓝: timeNap)
                (L: local) (G: global) (c: config).

  (** Wellformedness of program local step *)
  (* memory wf *)
  Lemma alloc_step_mem_wf 𝓥1 M1 l n 𝑚s 𝓥2 M2
    (ALLOC: alloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2) (WF: Wf M1) :
    Wf M2.
  Proof. inversion ALLOC. inversion MEMALL. by eapply wf_mem_list_addins. Qed.

  Lemma dealloc_step_mem_wf 𝓥1 M1 l n 𝑚s 𝓥2 M2
    (DEALLOC: dealloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2) (WF: Wf M1) :
    Wf M2.
  Proof. inversion DEALLOC. inversion MEMALL. by eapply wf_mem_list_addins. Qed.

  Lemma write_step_mem_wf L1 M1 𝑚 o b V L2 M2
    (WRITE: write_step L1 M1 𝑚 o b V L2 M2) (WF: Wf M1):
    Wf M2.
  Proof. inversion WRITE. by eapply memory_write_wf. Qed.

  Lemma program_step_mem_wf b c1 ev c2
    (STEP: program_step b c1 ev c2) (WF: Wf c1.(gb).(mem)) :
    Wf c2.(gb).(mem).
  Proof.
    inversion STEP; simpl; auto;
      [by eapply alloc_step_mem_wf|by eapply dealloc_step_mem_wf
      |by eapply write_step_mem_wf|by eapply write_step_mem_wf|by subst].
  Qed.

  (* threadView closed *)
  Lemma read_step_closed_tview L1 M tr 𝑚 o L2
    (READ: read_step L1 M tr 𝑚 o L2)
    (CLOSED: L1.(tv) ∈ M) (WF: Wf M) :
    L2.(tv) ∈ M.
  Proof.
    inversion READ. eapply read_helper_closed_tview; eauto.
    destruct (mrel (mbase 𝑚)) as [V|] eqn:HR; last done.
    have ?: Some V ∈ M by rewrite -HR; eapply WF. done.
  Qed.

  Lemma write_step_closed_tview L1 M1 𝑚 o b V L2 M2
    (WRITE: write_step L1 M1 𝑚 o b V L2 M2)
    (CLOSED: L1.(tv) ∈ M1) (WF: Wf M1) :
    L2.(tv) ∈ M2.
  Proof. inversion WRITE. eapply write_helper_closed_tview; eauto. Qed.

  Lemma alloc_step_closed_tview 𝓥1 M1 l n 𝑚s 𝓥2 M2
    (ALLOC: alloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2)
    (CLOSED: 𝓥1 ∈ M1) (WF: Wf M1):
         𝓥2 ∈ M2.
  Proof.
    inversion ALLOC. inversion MEMALL.
    eapply alloc_helper_mem_closed_tview; eauto. by apply AMES.
  Qed.

  Lemma dealloc_step_closed_tview 𝓥1 M1 l n 𝑚s 𝓥2 M2
    (DEALLOC: dealloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2)
    (CLOSED: 𝓥1 ∈ M1) (WF: Wf M1):
         𝓥2 ∈ M2.
  Proof.
    inversion DEALLOC. inversion MEMALL.
    eapply alloc_helper_mem_closed_tview; eauto. by apply DMES.
  Qed.

  Lemma acq_fence_step_closed_tview 𝓥 𝓥' M
    (ACQ: acq_fence_step 𝓥 𝓥') (CLOSED: 𝓥 ∈ M) :
    𝓥' ∈ M.
  Proof. inversion ACQ. constructor; apply CLOSED. Qed.

  Lemma rel_fence_step_closed_tview L1 L2 M
    (REL: rel_fence_step L1 L2) (CLOSED: L1.(tv) ∈ M) :
    L2.(tv) ∈ M.
  Proof. inversion REL. constructor=>/=; apply CLOSED. Qed.

  Lemma sc_fence_step_closed_tview L1 L2 M 𝓢 𝓢'
    (SC: sc_fence_step L1 𝓢 L2 𝓢') (CLOSED: L1.(tv) ∈ M) (CLOSED2: 𝓢 ∈ M):
    L2.(tv) ∈ M.
  Proof. inversion SC. eapply sc_fence_helper_closed_tview; eauto. Qed.

  Lemma program_step_closed_tview b c1 ev c2
    (STEP: program_step b c1 ev c2)
    (WF: Wf c1.(gb)) (CLOSED: c1.(lc).(tv) ∈ c1.(gb).(mem)) :
    c2.(lc).(tv) ∈ c2.(gb).(mem).
  Proof.
    inversion STEP; simpl; [..|by subst].
    - eapply alloc_step_closed_tview; eauto; apply WF.
    - eapply dealloc_step_closed_tview; eauto; apply WF.
    - eapply read_step_closed_tview; eauto; apply WF.
    - eapply write_step_closed_tview; eauto; apply WF.
    - eapply write_step_closed_tview; eauto; last apply WF.
      eapply read_step_closed_tview; eauto; apply WF.
    - eapply acq_fence_step_closed_tview; eauto.
    - eapply rel_fence_step_closed_tview; eauto.
    - eapply sc_fence_step_closed_tview; eauto; apply WF.
    - eapply sc_fence_step_closed_tview; eauto; apply WF.
  Qed.

  (* sc closed *)
  Lemma alloc_step_closed_timenap 𝓥1 M1 l n 𝑚s 𝓥2 M2 (T : timeNap)
    (ALLOC: alloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2)
    (CLOSED: T ∈ M1) (WF: Wf M1):
    T ∈ M2.
  Proof.
    inversion ALLOC. inversion MEMALL.
    eapply closed_timenap_list_addins_mono; eauto.
  Qed.

  Lemma dealloc_step_closed_timenap 𝓥1 M1 l n 𝑚s 𝓥2 M2 (T : timeNap)
    (DEALLOC: dealloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2)
    (CLOSED: T ∈ M1) (WF: Wf M1):
    T ∈ M2.
  Proof.
    inversion DEALLOC. inversion MEMALL.
    eapply closed_timenap_list_addins_mono; eauto.
  Qed.

  Lemma write_step_closed_timenap L1 M1 𝑚 o b V L2 M2 (T : timeNap)
    (WRITE: write_step L1 M1 𝑚 o b V L2 M2)
    (CLOSED: T ∈ M1) (WF: Wf M1) :
    T ∈ M2.
  Proof.
    inversion WRITE. eapply memory_write_closed_timenap; eauto; apply WF.
  Qed.

  Lemma program_step_closed_timenap b c1 ev c2 (T : timeNap)
    (STEP: program_step b c1 ev c2)
    (CLOSED: T ∈ c1.(gb).(mem)) (WF: Wf c1.(gb).(mem)) :
    T ∈ c2.(gb).(mem).
  Proof.
    inversion STEP; simpl; [| |done|..|done|done|done|done|by subst].
    - eapply alloc_step_closed_timenap; eauto.
    - eapply dealloc_step_closed_timenap; eauto.
    - eapply write_step_closed_timenap; eauto.
    - eapply write_step_closed_timenap; eauto.
  Qed.

  Lemma sc_fence_step_closed_sc L1 L2 M 𝓢 𝓢'
    (SC: sc_fence_step L1 𝓢 L2 𝓢') (CLOSED: L1.(tv) ∈ M) (CLOSED2: 𝓢 ∈ M):
    𝓢' ∈ M.
  Proof. inversion SC. eapply sc_fence_helper_closed_sc; eauto. Qed.

  Lemma program_step_closed_sc b c1 ev c2
    (STEP: program_step b c1 ev c2)
    (WF: Wf c1.(gb)) (CLOSED: c1.(lc).(tv) ∈ c1.(gb).(mem)) :
    c2.(gb).(sc) ∈ c2.(gb).(mem).
  Proof.
    inversion STEP; simpl; [| |apply WF| | |apply WF|apply WF|..|subst; apply WF].
    - eapply alloc_step_closed_timenap; eauto; apply WF.
    - eapply dealloc_step_closed_timenap; eauto; apply WF.
    - eapply write_step_closed_timenap; eauto; apply WF.
    - eapply write_step_closed_timenap; eauto; apply WF.
    - eapply sc_fence_step_closed_sc; eauto. apply WF.
    - eapply sc_fence_step_closed_sc; eauto. apply WF.
  Qed.

  (* threadView sqsubseteq *)
  Lemma acq_fence_step_tview_sqsubseteq 𝓥 𝓥'
    (ACQ: acq_fence_step 𝓥 𝓥') :
    𝓥 ⊑ 𝓥'.
  Proof. inversion ACQ. constructor; [done|done|by apply cur_acq|done]. Qed.

  Lemma rel_fence_step_tview_sqsubseteq L1 L2
    (REL: rel_fence_step L1 L2) :
    L1.(tv) ⊑ L2.(tv).
  Proof. inversion REL. constructor=>//=. apply frel_cur. Qed.

  Lemma sc_fence_helper_tview_sqsubseteq 𝓥 𝓥' 𝓢 𝓢'
    (SC: sc_fence_helper 𝓥 𝓢 𝓥' 𝓢') :
    𝓥 ⊑ 𝓥'.
  Proof.
    inversion SC.
    set T' := 𝓥.(acq).(rlx) ⊔ 𝓢.
    set V' := mkView_same T'.
    have ?: acq 𝓥 ⊑ V'.
    { constructor; rewrite /V' /T' /mkView_same /= ?pln_rlx; solve_lat. }
    subst 𝓢'. constructor; [done|by rewrite frel_cur cur_acq|by rewrite cur_acq|done].
  Qed.

  Lemma sc_fence_step_tview_sqsubseteq L1 𝓢1 L2 𝓢2
    (SC: sc_fence_step L1 𝓢1 L2 𝓢2) :
    L1.(tv) ⊑ L2.(tv).
  Proof. inversion SC. by eapply sc_fence_helper_tview_sqsubseteq. Qed.

  Lemma alloc_step_tview_sqsubseteq 𝓥1 M1 l n 𝑚s 𝓥2 M2
    (ALLOC: alloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2) :
    𝓥1 ⊑ 𝓥2.
  Proof. inversion ALLOC. by eapply alloc_helper_tview_sqsubseteq. Qed.

  Lemma dealloc_step_tview_sqsubseteq 𝓥1 M1 l n 𝑚s 𝓥2 M2
    (DEALLOC: dealloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2):
    𝓥1 ⊑ 𝓥2.
  Proof. inversion DEALLOC. by eapply alloc_helper_tview_sqsubseteq. Qed.

  Lemma read_step_tview_sqsubseteq L1 M1 tr 𝑚 o L2
    (READ: read_step L1 M1 tr 𝑚 o L2) :
    L1.(tv) ⊑ L2.(tv).
  Proof. inversion READ. by eapply read_helper_tview_sqsubseteq. Qed.

  Lemma write_step_tview_sqsubseteq L1 M1 𝑚 o b V L2 M2
    (WRITE: write_step L1 M1 𝑚 o b V L2 M2):
    L1.(tv) ⊑ L2.(tv).
  Proof. inversion WRITE. by eapply write_helper_tview_sqsubseteq. Qed.

  Lemma program_step_tview_sqsubseteq b c1 ev c2
    (STEP: program_step b c1 ev c2):
    c1.(lc).(tv) ⊑ c2.(lc).(tv).
  Proof.
    inversion STEP; [..|by subst].
    - by eapply alloc_step_tview_sqsubseteq.
    - by eapply dealloc_step_tview_sqsubseteq.
    - by eapply read_step_tview_sqsubseteq.
    - by eapply write_step_tview_sqsubseteq.
    - etrans; [by eapply read_step_tview_sqsubseteq|by eapply write_step_tview_sqsubseteq].
    - by apply acq_fence_step_tview_sqsubseteq.
    - by apply rel_fence_step_tview_sqsubseteq.
    - by eapply sc_fence_step_tview_sqsubseteq.
    - by eapply sc_fence_step_tview_sqsubseteq.
  Qed.

  (* promises sub *)
  Lemma program_step_promises_subset b c1 ev c2
    (STEP: program_step b c1 ev c2)
    (WF: Wf c1) :
    ((⊆) : SubsetEq (gmap _ _)) c2.(lc).(prm) c2.(gb).(mem).
  Proof.
    inversion STEP; simpl; auto.
    - inversion ALLOC; clear ALLOC; subst. inversion MEMALL.
      eapply mem_list_addins_subset; eauto; apply WF.
    - inversion DEALLOC. inversion MEMALL.
      eapply mem_list_addins_subset; eauto; apply WF.
    - inversion READ. apply WF.
    - inversion WRITE. eapply memory_write_subset_promise; eauto. apply WF.
    - inversion WRITE. eapply memory_write_subset_promise; eauto.
      inversion READ. apply WF.
    - apply WF.
    - inversion FREL. apply WF.
    - inversion FSC. apply WF.
    - inversion FSC. apply WF.
    - subst. apply WF.
  Qed.

  (* na closed *)
  Lemma memory_write_closed_na_timenap
    P1 (M1: memory) 𝑚 P2 M2 o k b (𝓝: timeNap) 𝓥  𝓝'
    (WRITE: memory_write P1 M1 𝑚 P2 M2 k b)
    (DRF: drf_write 𝑚 o 𝓝 𝓥  M1 𝓝')
    (WF: Wf M1) (CLOSED: 𝓝 ∈ M1): 𝓝' ∈ M2.
  Proof.
    inversion DRF.
    case_match; subst; [by eapply memory_write_closed_timenap|].
    destruct POSTG as [POSTG MAX]; simplify_eq.
    (* move: MAX => [_ ->] l t. *)
    move => l t.
    case (decide (l = 𝑚.(mloc))) => [->|NEq].
    - rewrite /set_write_time /timeNap_lookup_write lookup_partial_alter.
      move/fmap_Some => [[t'?]/= [/fmap_Some [? [? [? ?]]] ->]]. simplify_eq.
      do 2 eexists. split; last by eapply memory_write_new. done.
    - rewrite /set_write_time /timeNap_lookup_write lookup_partial_alter_ne //.
      move/fmap_Some => [[[??]?]/= [? ->]].
      by eapply memory_write_closed_timenap, timeNap_lookup_w.
  Qed.

  Lemma alloc_step_closed_na 𝓥1 M1 l n 𝑚s 𝓥2 M2 𝓝
    (ALLOC: alloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2) (WF: Wf M1) (CLOSED: 𝓝 ∈ M1):
    alloc_new_na 𝓝 𝑚s ∈ M2.
  Proof.
    inversion ALLOC. inversion MEMALL.
    eapply closed_na_timenap_list_addins; eauto.
  Qed.

  Lemma dealloc_step_closed_na 𝓥1 M1 l n 𝑚s 𝓥2 M2 𝓝1
    (DEALLOC: dealloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2) (WF: Wf M1) (CLOSED: 𝓝1 ∈ M1)
    (DRF: drf_dealloc 𝓥1 M1 𝑚s 𝓝1) :
    alloc_new_na 𝓝1 𝑚s ∈ M2.
  Proof.
    inversion DEALLOC. inversion MEMALL. inversion DRF. simplify_eq.
    eapply closed_na_timenap_list_addins; eauto.
  Qed.

  Lemma read_step_closed_na L1 M1 tr 𝑚 o L2 𝓝1 𝓝2
    (READ: read_step L1 M1 tr 𝑚 o L2)
    (DRF: drf_read 𝑚 o tr 𝓝1 L1.(tv) M1 𝓝2) (CLOSED: 𝓝1 ∈ M1):
    𝓝2 ∈ M1.
  Proof.
    inversion READ. inversion DRF.
    case_decide; subst. by apply add_aread_id_memory. by apply add_nread_id_memory.
  Qed.

  Lemma write_step_closed_na L1 M1 𝑚 o b V L2 M2 𝓝1 𝓝2
    (WRITE: write_step L1 M1 𝑚 o b V L2 M2)
    (DRF: drf_write 𝑚 o 𝓝1 L1.(tv) M1 𝓝2)
    (WF: Wf M1) (CLOSED: 𝓝1 ∈ M1):
    𝓝2 ∈ M2.
  Proof. inversion WRITE. eapply memory_write_closed_na_timenap; eauto. Qed.

  Lemma program_step_closed_na b c1 ev c2
    (STEP: program_step b c1 ev c2)
    (CLOSED: c1.(gb).(na) ∈ c1.(gb).(mem)) (WF: Wf c1.(gb).(mem)):
    c2.(gb).(na) ∈ c2.(gb).(mem).
  Proof.
    inversion STEP; auto;
      [by eapply alloc_step_closed_na|by eapply dealloc_step_closed_na
      |by eapply read_step_closed_na|by eapply write_step_closed_na| |by subst].
    (* CAS *)
    eapply write_step_closed_na; eauto.
    by eapply read_step_closed_na.
  Qed.

  (* alloc_inv *)
  Lemma alloc_step_alloc_inv 𝓥1 M1 l n 𝑚s 𝓥2 M2
    (ALLOC: alloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2)
    (AINV: alloc_inv M1):
    alloc_inv M2.
  Proof. inversion ALLOC. by eapply memory_alloc_alloc_inv. Qed.

  Lemma dealloc_step_alloc_inv 𝓥1 M1 l n 𝑚s 𝓥2 M2
    (DEALLOC: dealloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2)
    (AINV: alloc_inv M1) (WF: Wf M1):
    alloc_inv M2.
  Proof. inversion DEALLOC. by eapply memory_dealloc_alloc_inv. Qed.

  Lemma write_step_alloc_inv L1 M1 𝑚 o b V L2 M2
    (WRITE: write_step L1 M1 𝑚 o b V L2 M2)
    (AINV: alloc_inv M1) (WF: Wf M1):
    alloc_inv M2.
  Proof. inversion WRITE. by eapply memory_write_alloc_inv. Qed.

  Lemma program_step_alloc_inv b c1 ev c2
    (STEP: program_step b c1 ev c2)
    (AINV: alloc_inv c1.(gb).(mem)) (WF: Wf c1.(gb).(mem)):
    alloc_inv c2.(gb).(mem).
  Proof.
    inversion STEP; auto;
      [by eapply alloc_step_alloc_inv|by eapply dealloc_step_alloc_inv
      |by eapply write_step_alloc_inv|by eapply write_step_alloc_inv|by subst].
  Qed.

  (* dealloc_na_agree *)
  Lemma memory_promise_dealloc_na P1 M1 𝑚 P2 M2 k b 𝓝
    (PROMISE: promise P1 M1 𝑚 P2 M2 k b)
    (AGREE: dealloc_na_agree M1 𝓝) :
    dealloc_na_agree M2 𝓝.
  Proof.
    inversion PROMISE; move => l t m;
      case (decide ((l, t) = (mloc 𝑚, mto 𝑚))) => [Eq|NEq].
    - rewrite Eq (lookup_mem_addins_new _ _ _ _ MEM) => [[<-]]. by inversion ISVAL.
    - rewrite -(lookup_mem_addins_old_eq _ _ _ _ _ _ MEM NEq). by apply AGREE.
    - rewrite Eq (lookup_mem_split_new _ _ _ _ MEM) => [[<-]]. by inversion ISVAL.
    - move => In.
      move: (lookup_mem_split_old_rev _ _ _ _ _ _ _ In NEq MEM)
        => [m' [In' [_ Eq']]]. rewrite -Eq'. by apply AGREE.
    - rewrite Eq (lookup_mem_lower_new _ _ _ _ MEM) => [[<-]].
      inversion Eq. move : (lookup_mem_lower_old_rev _ _ _ _ MEM) => [? [EV _]].
      apply (AGREE _ _ _ EV).
    - rewrite -(lookup_mem_lower_old_eq _ _ _ _ _ _ MEM NEq). by apply AGREE.
  Qed.

  Lemma memory_write_dealloc_na o P1 M1 𝑚 P2 M2 k b 𝓝1 𝓝2 𝓥
    (WRITE: memory_write P1 M1 𝑚 P2 M2 k b)
    (DRF: drf_write 𝑚 o 𝓝1 𝓥 M1 𝓝2)
    (CLOSED: 𝓝1 ∈ M1)
    (AGREE: dealloc_na_agree M1 𝓝1):
    dealloc_na_agree M2 𝓝2.
  Proof.
    inversion_clear WRITE. clear REMOVE. inversion_clear DRF.
    case_match; [|destruct POSTG as [MAX POSTG]]; simplify_eq.
    - subst. by eapply memory_promise_dealloc_na.
    - move => l t m Eqm EqV.
      etrans; first by eapply memory_promise_dealloc_na.
      rewrite /timeNap_lookup_write /set_write_time.
      case (decide (l = 𝑚.(mloc))) => [->|NEq];
        [rewrite lookup_partial_alter|by rewrite lookup_partial_alter_ne].
      case H𝓝: (𝓝1 !! mloc 𝑚) => [[[??]?]/=|]; last done.
      apply timeNap_lookup_w in H𝓝.
      destruct (CLOSED _ _ H𝓝) as [mn [tn' [Le Eqmm]]].
      transitivity (Some tn')=>//. apply Qclt_le_weak.
      inversion PREW. case_decide; last by destruct o.
      by apply (MAX (mkMsg 𝑚.(mloc) tn' mn)).
  Qed.

  Lemma mem_list_addins_dealloc_na 𝑚s M1 M2 𝓝
    (ADDINS: mem_list_addins 𝑚s M1 M2)
    (ND: ∀ 𝑚, 𝑚 ∈ 𝑚s → 𝓝 !!w 𝑚.(mloc) ⊑ Some 𝑚.(mto))
    (DISJ: mem_list_disj 𝑚s)
    (AGREE: dealloc_na_agree M1 𝓝) :
    dealloc_na_agree M2 (alloc_new_na 𝓝 𝑚s).
  Proof.
    revert M2 ADDINS.
    induction 𝑚s as [|𝑚 𝑚s IH] => M2 ADDINS; inversion ADDINS; subst; [done|].
    move => l t m /=.
    case (decide ((l, t) = (mloc 𝑚, mto 𝑚))) => [Eq|NEq].
    - rewrite Eq (lookup_mem_addins_new _ _ _ _ ADD) => [[<-]].
      inversion Eq. by rewrite /timeNap_lookup_write lookup_insert.
    - rewrite -(lookup_mem_addins_old_eq _ _ _ _ _ _ ADD NEq).
      have IH2: dealloc_na_agree M3 (alloc_new_na 𝓝 𝑚s).
      { apply IH; [|by eapply mem_list_disj_cons|done].
        move => ??. apply ND. by right. }
      etrans; first by eapply IH2.
      rewrite /timeNap_lookup_write.
      case (decide (l = 𝑚.(mloc))) => [Eql|NEql];
        [rewrite Eql lookup_insert| by rewrite lookup_insert_ne].
      rewrite alloc_new_na_lookup_old.
      + apply ND. by left.
      + by apply mem_list_disj_cons_rest.
  Qed.

  Lemma alloc_dealloc_na  𝓥1 M1 l n 𝑚s 𝓥2 M2 𝓝
    (ALLOC: alloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2)
    (AGREE: dealloc_na_agree M1 𝓝) (CLOSED: 𝓝 ∈ M1):
    dealloc_na_agree M2 (alloc_new_na 𝓝 𝑚s).
  Proof.
    inversion_clear ALLOC.
    have DISJ := memory_alloc_disjoint _ _ _ _ _ MEMALL.
    inversion_clear MEMALL.
    eapply (mem_list_addins_dealloc_na _ _ _ _ ADD); [|done|done].
    move => 𝑚 /elem_of_list_lookup [n' In𝑚].
    destruct (𝓝 !!w 𝑚.(mloc)) as [t|] eqn:H𝓝; last done.
    apply CLOSED in H𝓝 as [? [? [_ Eqt]]].
    have Lt: (n' < n)%nat by rewrite -LEN; eapply lookup_lt_Some. exfalso.
    apply (alloc_add_fresh _ _ _ ALLOC _ Lt), memory_loc_elem_of_dom.
    destruct (AMES _ _ In𝑚) as [Eql _]. rewrite -Eql.
    intros EQ. by rewrite memory_lookup_cell EQ in Eqt.
  Qed.

  Lemma dealloc_dealloc_na  𝓥1 M1 l n 𝑚s 𝓥2 M2 𝓝
    (DEALLOC: dealloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2)
    (AGREE: dealloc_na_agree M1 𝓝) (CLOSED: 𝓝 ∈ M1):
    dealloc_na_agree M2 (alloc_new_na 𝓝 𝑚s).
  Proof.
    inversion_clear DEALLOC.
    have DISJ := memory_dealloc_disjoint _ _ _ _ _ MEMALL.
    inversion MEMALL.
    eapply (mem_list_addins_dealloc_na _ _ _ _ ADD); [|done|done].
    move => 𝑚 In𝑚.
    destruct (𝓝 !!w 𝑚.(mloc)) as [t|] eqn:H𝓝; last done.
    apply CLOSED in H𝓝 as [? [t' [Le Eqt]]].
    transitivity (Some t')=>//. apply Qclt_le_weak.
    by apply (memory_dealloc_max _ _ _ _ _ MEMALL _ In𝑚 (mkMsg 𝑚.(mloc) t' _) Eqt).
  Qed.

  Lemma read_step_dealloc_na L1 M1 tr 𝑚 o L2 𝓝1 𝓝2
    (READ: read_step L1 M1 tr 𝑚 o L2)
    (DRF: drf_read 𝑚 o tr 𝓝1 L1.(tv) M1 𝓝2)
    (AGREE: dealloc_na_agree M1 𝓝1) (CLOSED: 𝓝1 ∈ M1) :
    dealloc_na_agree M1 𝓝2.
  Proof.
    inversion READ. inversion DRF.
    case_decide; subst;
    move => ???; [rewrite add_aread_id_write|rewrite add_nread_id_write]; by apply AGREE.
  Qed.

  Lemma write_step_dealloc_na L1 M1 𝑚 o b V L2 M2 𝓝1 𝓝2
    (WRITE: write_step L1 M1 𝑚 o b V L2 M2)
    (DRF: drf_write 𝑚 o 𝓝1 L1.(tv) M1 𝓝2)
    (AGREE: dealloc_na_agree M1 𝓝1) (CLOSED: 𝓝1 ∈ M1) :
    dealloc_na_agree M2 𝓝2.
  Proof. inversion WRITE. by eapply memory_write_dealloc_na. Qed.

  Lemma program_step_dealloc_na b c1 ev c2
    (STEP: program_step b c1 ev c2)
    (AGREE: dealloc_na_agree c1.(gb).(mem) c1.(gb).(na))
    (CLOSED: c1.(gb).(na) ∈ c1.(gb).(mem)):
    dealloc_na_agree c2.(gb).(mem) c2.(gb).(na).
  Proof.
    inversion STEP; auto;
      [by eapply alloc_dealloc_na|(* by eapply dealloc_dealloc_na *)
      |by eapply read_step_dealloc_na|by eapply write_step_dealloc_na|(* by eapply write_step_dealloc_na *)|by subst].
    - cbn.                             (* dealloc *)
      inversion DRF; simplify_eq.
      by eapply dealloc_dealloc_na.
    - cbn.                      (* cas *)
      eapply write_step_dealloc_na; eauto.
      + by eapply read_step_dealloc_na.
      + by eapply read_step_closed_na.
  Qed.

  Lemma write_step_global_wf 𝑚 o σ σ' (Vr: view) (L L': local)
    (WRITE: write_step L σ.(mem) 𝑚 o true Vr L' σ'.(mem))
    (DRF: drf_write 𝑚 o σ.(na) L.(tv) σ.(mem) σ'.(na))
    (CLOSED: L.(tv) ∈ σ.(mem))
    (EQSC: σ'.(sc) = σ.(sc)) (WF: Wf σ):
    Wf σ'.
  Proof.
    constructor.
    - eapply write_step_mem_wf; [by eauto|by apply WF].
    - eapply write_step_alloc_inv; [by eauto|by apply WF..].
    - eapply write_step_dealloc_na; [by eauto|by eauto|by apply WF..].
    - rewrite EQSC. eapply write_step_closed_timenap; [by eauto|by apply WF..].
    - eapply write_step_closed_na; [by eauto|by eauto|by apply WF..].
  Qed.

  Lemma program_step_global_wf b c1 ev c2
    (STEP: program_step b c1 ev c2)
    (WF: Wf c1.(gb)) (CLOSED: c1.(lc).(tv) ∈ c1.(gb).(mem)) :
    Wf c2.(gb).
  Proof.
    constructor.
    - eapply program_step_mem_wf; eauto; apply WF.
    - eapply program_step_alloc_inv; eauto; apply WF.
    - eapply program_step_dealloc_na; eauto; apply WF.
    - eapply program_step_closed_sc; eauto; apply WF.
    - eapply program_step_closed_na; eauto; apply WF.
  Qed.

  Lemma program_step_config_wf b c1 ev c2
    (STEP: program_step b c1 ev c2) (WF: Wf c1) :
    Wf c2.
  Proof.
    constructor.
    - eapply program_step_global_wf; eauto; apply WF.
    - by eapply program_step_closed_tview; eauto; apply WF.
    - by eapply program_step_promises_subset.
  Qed.

  Lemma program_step_no_promise b c1 ev c2
    (STEP: program_step b c1 ev c2) (WF: Wf c1)
    (EMPTY: c1.(lc).(prm) = ∅) :
    c2.(lc).(prm) = ∅.
  Proof.
    inversion STEP; simpl; auto.
    - by inversion READ.
    - inversion WRITE. by eapply memory_write_empty_promise.
    - inversion WRITE.
      eapply memory_write_empty_promise; eauto. by inversion READ.
    - by inversion FREL.
    - by inversion FSC.
    - by inversion FSC.
    - by subst.
  Qed.

  (** WF for promise step *)
  Lemma promise_step_config_wf b 𝑚 k c1 c2
    (STEP: promise_step b 𝑚 k c1 c2) (WF: Wf c1) :
    Wf c2.
  Proof.
    inversion STEP. constructor; first constructor; simpl.
    - eapply memory_promise_wf; eauto. apply WF.
    - eapply memory_promise_alloc_inv; eauto; apply WF.
    - eapply memory_promise_dealloc_na; eauto. apply WF.
    - eapply memory_promise_closed_timenap; eauto; apply WF.
    - eapply memory_promise_closed_timenap; eauto; apply WF.
    - constructor.
      + move => ?. eapply memory_promise_opt_closed_view; eauto; apply WF.
      + eapply memory_promise_closed_view; eauto; apply WF.
      + eapply memory_promise_closed_view; eauto; apply WF.
      + eapply memory_promise_closed_view; eauto; apply WF.
    - eapply promise_subset; eauto. apply WF.
  Qed.

  Lemma promise_step_msg_view_closed b 𝑚 k c1 c2
    (PROMISE: promise_step b 𝑚 k c1 c2) :
    𝑚.(mbase).(mrel) ∈ c2.(gb).(mem).
  Proof. inversion PROMISE. by inversion PROMISE0. Qed.

  (** WF for thread steps *)
  Lemma thread_step_config_wf pr pf c1 ev c2
    (TSTEP: thread_step pr pf c1 ev c2) (WF: Wf c1):
    Wf c2.
  Proof.
    inversion TSTEP; subst.
    - by eapply promise_step_config_wf.
    - by eapply program_step_config_wf.
  Qed.

  Lemma thread_step_no_promise pf c1 ev c2
    (TSTEP: thread_step false pf c1 ev c2) (WF: Wf c1)
    (EMPTY: c1.(lc).(prm) = ∅) :
    c2.(lc).(prm) = ∅.
  Proof. inversion TSTEP. by eapply program_step_no_promise. Qed.
End Wellformedness.


Section AllocSteps.
  Context `{LocFacts loc} `{CVAL: Countable VAL} `{Shift loc}
          `{!Allocator (state:=@memory loc _ VAL)}.

  Notation memory := (@memory _ _ VAL).
  Notation message := (@message _ _ VAL).
  Notation event := (@event loc VAL).
  Notation program_step := (@program_step _ _ VAL _ _).

  Implicit Type (𝑚: message) (M: memory) (𝓝: timeNap).

  (* Lifting lemmas to alloc step level *)
  Lemma alloc_step_mem_fresh  𝓥1 M1 l n 𝑚s 𝓥2 M2
     (ALLOC: alloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2):
    ∀ (n' : nat), (n' < n)%nat → l >> n' ∈ dom (gset loc) M2 ∖ dom (gset loc) M1.
  Proof. inversion ALLOC. by eapply memory_alloc_fresh. Qed.

  Lemma alloc_step_mem_fresh_2  𝓥1 M1 l n 𝑚s 𝓥2 M2
     (ALLOC: alloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2):
    ∀ 𝑚, 𝑚 ∈ 𝑚s → 𝑚.(mloc) ∉ dom (gset loc) M1.
  Proof. inversion ALLOC. by eapply memory_alloc_fresh_2. Qed.

  Lemma alloc_step_cell_list_lookup  𝓥1 M1 l n 𝑚s 𝓥2 M2
     (ALLOC: alloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2):
    ∀ (n': nat) C,
      (cell_list l n M2) !! n' = Some C
      ↔ ∃ 𝑚, 𝑚s !! n' = Some 𝑚 ∧ C = {[𝑚.(mto) := 𝑚.(mbase)]}.
  Proof. inversion ALLOC. by eapply memory_alloc_cell_list. Qed.

  Lemma alloc_step_cell_list_map 𝓥1 M1 l n 𝑚s 𝓥2 M2
      (ALLOC: alloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2):
    (cell_list l n M2) = fmap (λ 𝑚, {[𝑚.(mto) := 𝑚.(mbase)]}) 𝑚s.
  Proof. inversion ALLOC. by eapply memory_alloc_cell_list_map. Qed.

  Lemma alloc_step_mem_lookup 𝓥1 M1 l n 𝑚s 𝓥2 M2
      (ALLOC: alloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2):
    ∀ 𝑚, 𝑚 ∈ 𝑚s
    → M2 !!c 𝑚.(mloc) = {[𝑚.(mto) := 𝑚.(mbase)]}.
  Proof. inversion ALLOC. by eapply memory_alloc_lookup. Qed.

  Lemma alloc_step_mem_insert 𝓥1 M1 l n 𝑚s 𝓥2 M2
      (ALLOC: alloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2):
    M2 = alloc_new_mem M1 𝑚s.
  Proof. inversion ALLOC. by eapply memory_alloc_insert. Qed.

  Lemma alloc_step_disjoint 𝓥1 M1 l n 𝑚s 𝓥2 M2
      (ALLOC: alloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2):
    mem_list_disj 𝑚s.
  Proof. inversion ALLOC. by eapply memory_alloc_disjoint. Qed.

  Lemma alloc_step_loc_eq 𝓥1 M1 l n 𝑚s 𝓥2 M2
      (ALLOC: alloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2):
    ∀ (n': nat) 𝑚, 𝑚s !! n' = Some 𝑚 → 𝑚.(mloc) = l >> n'.
  Proof. inversion ALLOC. by eapply memory_alloc_loc_eq. Qed.

  Lemma alloc_step_AVal 𝓥1 M1 l n 𝑚s 𝓥2 M2
      (ALLOC: alloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2):
    ∀ (n': nat) 𝑚, 𝑚s !! n' = Some 𝑚 → 𝑚.(mbase).(mval) = AVal.
  Proof. inversion ALLOC. by eapply memory_alloc_AVal. Qed.

  Lemma alloc_step_view_None 𝓥1 M1 l n 𝑚s 𝓥2 M2
      (ALLOC: alloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2):
    ∀ (n': nat) 𝑚, 𝑚s !! n' = Some 𝑚 → 𝑚.(mbase).(mrel) = None.
  Proof. inversion ALLOC. by eapply memory_alloc_view_None. Qed.

  Lemma alloc_step_length 𝓥1 M1 l n 𝑚s 𝓥2 M2
      (ALLOC: alloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2):
    length 𝑚s = n.
  Proof. inversion ALLOC. by eapply memory_alloc_length. Qed.

  Lemma alloc_step_mem_cut 𝓥1 M1 l n 𝑚s 𝓥2 M2 𝓝
      (ALLOC: alloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2):
    mem_cut M2 (alloc_new_na 𝓝 𝑚s) = alloc_new_mem (mem_cut M1 𝓝) 𝑚s.
  Proof. inversion ALLOC. by eapply mem_cut_memory_alloc. Qed.

  Lemma dealloc_step_disjoint 𝓥1 M1 l n 𝑚s 𝓥2 M2
    (DEALLOC: dealloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2) :
      mem_list_disj 𝑚s.
  Proof. inversion DEALLOC. by eapply memory_dealloc_disjoint. Qed.

  Lemma dealloc_step_remove 𝓥1 M1 l n 𝑚s 𝓥2 M2
    (DEALLOC: dealloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2) :
    ∀ (n' : nat), (n' < n)%nat
    → l >> n' ∈ (dom (gset loc) M1 ∖ mem_deallocated M1) ∩ mem_deallocated M2.
  Proof. inversion DEALLOC. by eapply memory_dealloc_remove. Qed.

  Lemma dealloc_step_loc_eq 𝓥1 M1 l n 𝑚s 𝓥2 M2
      (DEALLOC: dealloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2):
    ∀ (n': nat) 𝑚, 𝑚s !! n' = Some 𝑚 → 𝑚.(mloc) = l >> n'.
  Proof. inversion DEALLOC. by eapply memory_dealloc_loc_eq. Qed.

  Lemma dealloc_step_AVal 𝓥1 M1 l n 𝑚s 𝓥2 M2
      (DEALLOC: dealloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2):
    ∀ (n': nat) 𝑚, 𝑚s !! n' = Some 𝑚 → 𝑚.(mbase).(mval) = DVal.
  Proof. inversion DEALLOC. by eapply memory_dealloc_DVal. Qed.

  Lemma dealloc_step_length 𝓥1 M1 l n 𝑚s 𝓥2 M2
      (DEALLOC: dealloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2):
    length 𝑚s = n.
  Proof. inversion DEALLOC. by eapply memory_dealloc_length. Qed.

  Lemma dealloc_step_mem_cut 𝓥1 M1 l n 𝑚s 𝓥2 M2 𝓝
      (DEALLOC: dealloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2):
    mem_cut M2 (alloc_new_na 𝓝 𝑚s) = alloc_new_mem (mem_cut M1 𝓝) 𝑚s.
  Proof. inversion DEALLOC. by eapply mem_cut_memory_dealloc. Qed.

  (** Progress for alloc *)
  Definition alloc_messages (n: nat) (l : loc) : list message :=
    fmap (λ (i: nat), mkMsg (l >> i) 1%Qp (mkBMes AVal (1/2)%Qp None)) (seq 0%nat n).

  Definition alloc_new_tview (𝑚s: list message) 𝓥1:=
    foldr (λ 𝑚 𝓥, write_tview 𝓥 Plain 𝑚.(mloc) 𝑚.(mto)) 𝓥1 𝑚s.

  Lemma alloc_messages_cons n l :
    alloc_messages (S n) l =
       mkMsg (l >> 0) 1%Qp (mkBMes AVal (1/2)%Qp None) :: alloc_messages n (l >> 1).
  Proof.
    rewrite /alloc_messages /=. f_equal. simpl. apply list_eq => i.
    rewrite 2!list_lookup_fmap /=. case (decide (i < n)%nat) => [Lt| Ge].
    - do 2 (rewrite lookup_seq; last done). simpl.
      rewrite (_: l >> Z.pos (Pos.of_succ_nat i)
                  = l >> 1%nat >> Z.of_nat i); first done.
      rewrite shift_nat_assoc. by f_equal.
    - apply Nat.nlt_ge in Ge.
      do 2 (rewrite lookup_seq_ge; last done). done.
  Qed.

  Lemma alloc_messages_shift_1 n l :
    ∀ 𝑚 , 𝑚 ∈ alloc_messages n (l >> 1) → l >> 0 ≠ mloc 𝑚.
  Proof.
    move => 𝑚 /elem_of_list_fmap [i [-> In]] /=.
    rewrite (shift_nat_assoc _ 1) => /(shift_nat_inj _ 0). lia.
  Qed.

  Lemma alloc_memory_progress n l M1
    (FRESH: alloc M1 n l):
    let 𝑚s := alloc_messages n l in
    memory_alloc n l 𝑚s M1 (alloc_new_mem M1 𝑚s).
  Proof.
    move => 𝑚s.
    constructor; last done.
    - by rewrite map_length seq_length.
    - move => n' 𝑚 Eq.
      rewrite /𝑚s /alloc_messages in Eq.
      apply list_lookup_fmap_inv in Eq as [i [Eq1 [Eq2 Lt]%lookup_seq_inv]].
      simpl in Eq2. subst i.
      by rewrite Eq1 /=.
    - have FRESH' := alloc_add_fresh _ _ _ FRESH.
      rewrite /𝑚s. clear FRESH 𝑚s.
      revert l FRESH'.
      induction n as [|n IH] => l FRESH.
      + rewrite /alloc_messages /=. constructor.
      + rewrite alloc_messages_cons /=.
        have MA : mem_list_addins (alloc_messages n (l >> 1)) M1
                  (alloc_new_mem M1 (alloc_messages n (l >> 1))).
        { apply (IH (l >> 1)).
          move => n' Lt. rewrite (shift_nat_assoc _ 1). apply FRESH. by lia. }
        econstructor; [exact MA| |by constructor|done].
        econstructor. simpl.
        rewrite -(mem_list_addins_old _ _ _ _ MA).
        * rewrite (_ : _ !!c _ = ∅); first done.
          apply memory_loc_not_elem_of_dom, (FRESH O). by lia.
        * apply alloc_messages_shift_1.
  Qed.

  Lemma alloc_tview_progress n l M1 𝓥1
    (HC: 𝓥1 ∈ M1)
    (FRESH: alloc M1 n l):
    let 𝑚s := alloc_messages n l in
    alloc_helper 𝑚s 𝓥1 (alloc_new_tview 𝑚s 𝓥1).
  Proof.
    move => 𝑚s. rewrite /𝑚s.
    have FRESH' := alloc_add_fresh _ _ _ FRESH.
    clear FRESH 𝑚s. revert l FRESH'.
    induction n as [|n IH] => l FRESH.
    - rewrite /alloc_messages /=. constructor.
    - rewrite alloc_messages_cons /=.
      set 𝓥' := alloc_new_tview (alloc_messages n (l >> 1)) 𝓥1.
      have MA : alloc_helper (alloc_messages n (l >> 1)) 𝓥1 𝓥'.
      { apply (IH (l >> 1)).
        move => n' Lt. rewrite (shift_nat_assoc _ 1). apply FRESH. by lia. }
      clear IH.
      have HRlx : rlx (cur 𝓥') !! (l >> 0) = None. {
        rewrite -(alloc_helper_cur_old _ _ _ _ MA);
          last by apply alloc_messages_shift_1.
        apply (closed_timenap_memory_None _ M1); last apply HC.
        apply memory_loc_not_elem_of_dom, (FRESH O). by lia. }
      have HRel : rel 𝓥' !! (l >> 0) = None.
        { rewrite -(alloc_helper_rel_old _ _ _ _ MA);
            last by apply alloc_messages_shift_1.
          apply (not_elem_of_dom (D:=gset loc))=>/(rel_dom _ _) /elem_of_dom [[[t tRa] tR] Eq].
          destruct (closed_view_rlx _ _ (closed_tview_cur _ _ HC) _ _
                    (timeNap_lookup_w _ _ _ _ _ Eq))
            as [m [t' [_ Eqm]]].
          apply (FRESH O); first by lia. rewrite memory_lookup_cell in Eqm.
          apply memory_loc_elem_of_dom=>EQ. by rewrite EQ in Eqm. }
      econstructor; [exact MA|]. simpl.
      erewrite ->threadView_eq; [econstructor|..]=>//=.
      by rewrite (timeNap_lookup_w' _ _ _ HRlx); compute.
  Qed.

  Lemma alloc_progress c1 l n b
    (CLOSED: c1.(lc).(tv) ∈ c1.(gb).(mem))
    (ALLOC: alloc c1.(gb).(mem) (Pos.to_nat n) l):
    ∃ c2, program_step b c1 (Alloc l n) c2.
  Proof.
    eexists.
    set 𝑚s := alloc_messages (Pos.to_nat n) l.
    eapply (PStepA _ _ _ _ _ (alloc_new_mem c1.(gb).(mem) 𝑚s) 𝑚s).
    econstructor.
    - by apply alloc_memory_progress.
    - eapply alloc_tview_progress; eauto.
  Qed.

  (** Progress for dealloc *)
  Definition dealloc_messages (M: memory) (n: nat) (l : loc) : list message :=
    fmap (λ (i: nat),
            match cell_max (M !!c (l >> i)) with
            | Some (t,_) =>
                mkMsg (l >> i) (t+1)%Qp (mkBMes DVal t None)
            | _ =>
                mkMsg (l >> i) 1%Qp (mkBMes DVal (1/2)%Qp None)
            end)
         (seq 0%nat n).

  Definition dealloc_new_mem (M: memory) (𝑚s: list message) : memory :=
    foldr (λ 𝑚 M,
       <[𝑚.(mloc) := <[𝑚.(mto) := 𝑚.(mbase)]> (M !!c 𝑚.(mloc))]> M) M 𝑚s.

  Definition dealloc_new_tview (𝑚s: list message) 𝓥1:=
    foldr (λ 𝑚 𝓥, write_tview 𝓥 Plain 𝑚.(mloc) 𝑚.(mto)) 𝓥1 𝑚s.

  Lemma dealloc_messages_cons M n l :
    dealloc_messages M (S n) l =
       (match (cell_max (M !!c (l >> 0))) with
        | Some (t,_) =>
            mkMsg (l >> 0) (t+1)%Qp (mkBMes DVal t None)
        | _ =>
            mkMsg (l >> 0) 1%Qp (mkBMes DVal (1/2)%Qp None)
        end) :: dealloc_messages M n (l >> 1).
  Proof.
    rewrite /dealloc_messages /=. f_equal. apply list_eq => i.
    rewrite 2!list_lookup_fmap /=. case (decide (i < n)%nat) => [Lt| Ge].
    - do 2 (rewrite lookup_seq; last done). simpl.
      rewrite (_: l >> Z.pos (Pos.of_succ_nat i)
                  = l >> 1%nat >> Z.of_nat i); first done.
      rewrite shift_nat_assoc. by f_equal.
    - apply Nat.nlt_ge in Ge.
      do 2 (rewrite lookup_seq_ge; last done). done.
  Qed.

  Lemma dealloc_messages_shift_1 M n l :
    ∀ 𝑚 , 𝑚 ∈ dealloc_messages M n (l >> 1) → l >> 0 ≠ mloc 𝑚.
  Proof.
    move => 𝑚 /elem_of_list_fmap [i [-> In]] /=.
    case_match; first case_match; simpl;
    rewrite (shift_nat_assoc _ 1) => /(shift_nat_inj _ 0); lia.
  Qed.

  Lemma dealloc_messages_length M n l:
    length (dealloc_messages M n l) = n.
  Proof. by rewrite fmap_length seq_length. Qed.

  Lemma dealloc_messages_eq_loc M n l :
    ∀ (n': nat) 𝑚, (dealloc_messages M n l) !! n' = Some 𝑚 → 𝑚.(mloc) = l >> n'.
  Proof.
    move => n' 𝑚 Eq.
    have Lt: (n' < n)%nat.
    { apply lookup_lt_Some in Eq. move : Eq. by rewrite dealloc_messages_length. }
    move : Eq.
    rewrite list_lookup_fmap (lookup_seq _ _ _ Lt) /= => [[<-]].
    by case_match; [case_match|].
  Qed.

  Lemma dealloc_messages_eq_loc_2 M n l :
    ∀ 𝑚, 𝑚 ∈ (dealloc_messages M n l) →
      ∃ n':nat, (dealloc_messages M n l) !! n' = Some 𝑚 ∧ (n' < n)%nat ∧ 𝑚.(mloc) = l >> n'.
  Proof.
    move => 𝑚 /elem_of_list_lookup [n' Eqn'].
    exists n'. split; [done|split]; last by eapply dealloc_messages_eq_loc.
     apply lookup_lt_Some in Eqn'. by rewrite dealloc_messages_length in Eqn'.
  Qed.

  Lemma dealloc_messages_max M n l :
    ∀ 𝑚, 𝑚 ∈ (dealloc_messages M n l) →
      ∀ 𝑚', 𝑚' ∈ M → mloc 𝑚' = mloc 𝑚 → (mto 𝑚' < mto 𝑚)%Qc.
  Proof.
    move => 𝑚 /elem_of_list_fmap [i [-> In]] /= 𝑚' In' EQL.
    have EQLOC: 𝑚'.(mloc) = l >> i by case_match; [case_match|]. clear EQL.
    rewrite /elem_of /message_ElemOf memory_lookup_cell in In'.
    assert (∃ t0 m0, cell_max (M !!c mloc 𝑚') = Some (t0, m0)) as [t0 [m0 Eqm0]]
      by (eapply gmap_top_nonempty_2; eauto with typeclass_instances).
    rewrite -EQLOC. rewrite Eqm0.
    eapply Qcle_lt_trans; last by (apply Qp_lt_sum; eexists).
    eapply (gmap_top_top _ _ _ _ Eqm0), elem_of_dom_2, In'.
  Qed.

  Lemma dealloc_memory_progress n l (M: memory)
    (NEMP: ∀ (n':nat), (n' < n)%nat → M !!c (l >> n') ≠ ∅)
    (DEALLOC: dealloc M n l) (AINV: alloc_inv M):
    let 𝑚s := dealloc_messages M n l in
    memory_dealloc n l 𝑚s M (dealloc_new_mem M 𝑚s).
  Proof.
    move => 𝑚s.
    have REMOVE:= dealloc_remove _ _ _ DEALLOC.
    constructor; last done.
    - by rewrite map_length seq_length.
    - move => n' 𝑚 Eq. rewrite /𝑚s /dealloc_messages in Eq.
      apply list_lookup_fmap_inv in Eq as [i [Eq1 [Eq2 Lt]%lookup_seq_inv]].
      simpl in Eq2. subst i.
      move : (REMOVE _ Lt)
        => /elem_of_difference [/memory_loc_elem_of_dom Eqm NIN].
      assert (∃ t m, cell_max (M !!c (l >> n')) = Some (t, m)) as [t [m Eqmx]].
        by apply gmap_top_nonempty; eauto with typeclass_instances.
      rewrite Eqmx in Eq1. rewrite Eq1 /=.
      split; [done|split; [done|split; [done|split]]]; last first.
      { apply gmap_top_lookup in Eqmx; eauto with typeclass_instances.
        do 2 eexists. by rewrite memory_lookup_cell. }
      move => t' m' Eqm'. rewrite memory_lookup_cell in Eqm'.
      split.
      + move => EqD. apply NIN, mem_deallocated_correct2.
        apply cell_deallocated_correct2.
        exists t', m'. split; [done|split;[done|]].
        have MAX: cell_max (M !!c (l >> n')) = Some (t',m')
          by apply (alloc_inv_max_dealloc _ AINV).
        by apply (gmap_top_top _ _ _ _ MAX).
      + eapply Qcle_lt_trans; last by apply (Qp_lt_sum t (t+1)%Qp); eexists.
        by apply (gmap_top_top _ _ _ _ Eqmx), (elem_of_dom_2 _ _ _ Eqm').
    - clear DEALLOC. revert l 𝑚s NEMP REMOVE.
      induction n as [|n IH] => l 𝑚s NEMP REMOVE; first by constructor.
      have MA : mem_list_addins (dealloc_messages M n (l >> 1)) M
                                (dealloc_new_mem M (dealloc_messages M n (l >> 1))).
      { apply (IH (l >> 1));
          [move => ??|move => ??]; rewrite (shift_nat_assoc _ 1);
          [apply NEMP|apply REMOVE]; by lia. } clear IH.
      rewrite /𝑚s dealloc_messages_cons.
      econstructor; [exact MA|..].
      + assert (∃ t0 m0, cell_max (M !!c (l >> 0)) = Some (t0, m0)) as [t0 [m0 Eqm0]].
        { apply gmap_top_nonempty; eauto with typeclass_instances.
          by apply (NEMP O); lia. }
        have HL: M !!c (l >> 0)
          = dealloc_new_mem M (dealloc_messages M n (l >> 1)) !!c (l >> 0).
        { rewrite (mem_list_addins_old _ _ _ _ MA); first done.
          apply dealloc_messages_shift_1. }
        rewrite Eqm0. econstructor. rewrite -HL /=.
        constructor => ?? Eqm' ? [/= _ Le1] [/= /Qclt_nge Lt2 _].
        apply Lt2. etrans; first apply Le1.
        by apply (gmap_top_top _ _ _ _ Eqm0), (elem_of_dom_2 _ _ _ Eqm').
      + case_match; [case_match|]; (constructor; [..|done]);
          apply Qp_lt_sum; [by eexists|exists (1/2)%Qp; by rewrite Qp_div_2].
      + by case_match; [case_match|].
  Qed.

  Lemma dealloc_tview_progress n l M1 𝓥1
    (HC: 𝓥1 ∈ M1)
    (DEALLOC: dealloc M1 n l):
    let 𝑚s := dealloc_messages M1 n l in
    alloc_helper 𝑚s 𝓥1 (dealloc_new_tview 𝑚s 𝓥1).
  Proof.
    move => 𝑚s. rewrite /𝑚s.
    have REMOVE := dealloc_remove _ _ _ DEALLOC.
    clear DEALLOC 𝑚s. revert l REMOVE.
    induction n as [|n IH] => l REMOVE; first by constructor.
    rewrite dealloc_messages_cons.
    set 𝓥' := dealloc_new_tview (dealloc_messages M1 n (l >> 1)) 𝓥1.
    have MA : alloc_helper (dealloc_messages M1 n (l >> 1)) 𝓥1 𝓥'.
    { apply (IH (l >> 1)) => n' Lt. rewrite (shift_nat_assoc _ 1).
      apply REMOVE. by lia. } clear IH.
    econstructor; [exact MA|].
    econstructor=>//. remember (mloc _) as l'.
    rewrite -(_: rlx (cur 𝓥1) !!w l' = rlx (cur 𝓥') !!w l'); last first.
    { rewrite (timeNap_lookup_w' _ _ _ (alloc_helper_cur_old _ _ _ l' MA _)); [done|].
      subst l'. by case_match; [case_match|]; apply dealloc_messages_shift_1. }
    subst l'.
    destruct (𝓥1.(cur).(rlx) !! (l >> 0)) as [t0|] eqn:Ht0;
      last by (case_match; [case_match|]; rewrite /= (timeNap_lookup_w' _ _ _ Ht0); compute).
    apply timeNap_lookup_w' in Ht0.
    destruct (closed_view_rlx _ _ (closed_tview_cur _ _ HC) _ _ Ht0)
      as [mo [to [Leo Eqmo]]].
    rewrite memory_lookup_cell in Eqmo.
    assert (∃ tm mm, cell_max (M1 !!c (l >> 0)) = Some (tm, mm)) as [tm [mm Eqmm]].
    { by eapply gmap_top_nonempty_2; eauto with typeclass_instances. }
    rewrite Eqmm /= Ht0.
    eapply (strict_transitive_r _ (Some to)); first apply Leo.
    eapply (strict_transitive_r _ (Some tm)), total_not_strict, Qclt_not_le,
           Qp_lt_sum; last by eauto.
    apply (gmap_top_top _ _ _ _ Eqmm), (elem_of_dom_2 _ _ _ Eqmo).
  Qed.

  Lemma dealloc_progress c1 l n b
    (DEALLOC: dealloc c1.(gb).(mem) (Pos.to_nat n) l)
    (NEMP: ∀ n', (n' < Pos.to_nat n)%nat → c1.(gb).(mem) !!c (l >> n') ≠ ∅)
    (DRFB: ∀ n', (n' < Pos.to_nat n)%nat →
            c1.(gb).(na) !! (l >> n') ⊑ c1.(lc).(tv).(cur).(rlx) !! (l >> n'))
    (DRFW: ∀ n', (n' < Pos.to_nat n)%nat →
            ∀ 𝑚', 𝑚' ∈ c1.(gb).(mem) → mloc 𝑚' = l >> n' →
              Some (mto 𝑚') ⊑ c1.(lc).(tv).(cur).(pln) !!w (l >> n'))
    (AINV: alloc_inv c1.(gb).(mem))
    (CLOSED: c1.(lc).(tv) ∈ c1.(gb).(mem)) :
    ∃ c2, program_step b c1 (Dealloc l n) c2.
  Proof.
    eexists.
    set 𝑚s := dealloc_messages c1.(gb).(mem) (Pos.to_nat n) l.
    apply (PStepD _ _ _ _ 𝑚s (dealloc_new_tview 𝑚s c1.(lc).(tv))
                              (dealloc_new_mem c1.(gb).(mem) 𝑚s));
      first constructor.
    - by apply dealloc_memory_progress.
    - by apply dealloc_tview_progress.
    - constructor.
      + move => 𝑚 In𝑚. econstructor; eauto. simpl.
        destruct (dealloc_messages_eq_loc_2 _ _ _ _ In𝑚) as  [n' [Eq' [Lt' EqL]]].
        move => ? /(DRFW _ Lt') Lt EqL'. rewrite EqL' EqL in Lt. rewrite EqL.
        by apply Lt.
      + move => 𝑚 /dealloc_messages_eq_loc_2 [n' [Eq' [Lt' EqL]]].
        rewrite EqL. apply timeNap_sqsubseteq. by apply DRFB.
      + move => 𝑚 /dealloc_messages_eq_loc_2 [n' [Eq' [Lt' EqL]]].
        rewrite EqL. apply timeNap_sqsubseteq. by apply DRFB.
  Qed.
End AllocSteps.

Section Steps.
  Context `{LocFacts loc} `{CVAL: Countable VAL} `{Shift loc}
          `{!Allocator (state:=@memory loc _ VAL)}.

  Notation memory := (@memory _ _ VAL).
  Notation message := (@message _ _ VAL).
  Notation event := (@event loc VAL).
  Notation program_step := (@program_step _ _ VAL _ _).

  Implicit Types (M: memory) (𝑚: message).

  (* Lifting lemmas to step level *)
  Lemma write_step_addins_fresh L1 M1 𝑚 o V L2 M2
    (WRITE: write_step L1 M1 𝑚 o true V L2 M2) (WF: Wf M1) :
    M1 !! (𝑚.(mloc), 𝑚.(mto)) = None.
  Proof.
    inversion_clear WRITE. by eapply memory_write_addins_fresh.
  Qed.

  Lemma write_step_addins_eq L1 M1 𝑚 o V L2 M2
    (WRITE: write_step L1 M1 𝑚 o true V L2 M2) :
    M2 = <[mloc 𝑚:=<[mto 𝑚:=mbase 𝑚]> (M1 !!c mloc 𝑚)]> M1.
  Proof.
    inversion_clear WRITE. by eapply memory_write_addins_eq.
  Qed.

  (** Progress for read *)
  Definition read_new_tview (o : memOrder) l t tr (R: view) 𝓥 : threadView :=
    let V := if decide (Relaxed ⊑ o) then mkView_same {[l := (t,{[tr]},∅)]}
             else mkView_rlx {[l := (t,∅,{[tr]})]} in
    read_tview 𝓥 o R V.

  Lemma tview_closed_max (𝓥: threadView) M l tm mm
    (CLOSED: 𝓥 ∈ M) (MAX: cell_max (VAL:=VAL) (M !!c l) = Some (tm, mm)) :
      𝓥.(cur).(rlx) !!w l ⊑ Some tm.
  Proof.
    destruct (𝓥.(cur).(rlx) !!w l) as [ct|] eqn:Eqct; last done.
    destruct (closed_view_rlx _ _ (closed_tview_cur _ _ CLOSED ) _ _ Eqct)
        as [mt [tt [Lett Eqmt]]].
    cbn. transitivity (Some tt)=>//.
    apply (gmap_top_top _ _ _ _ MAX), elem_of_dom.
    eexists. by rewrite -memory_lookup_cell.
  Qed.

  Lemma read_step_progress l o M1 L1 𝓝1
    (CLOSED: L1.(tv) ∈ M1) (WFM: Wf M1)
    (AINV: alloc_inv M1) (ALLOC: allocated l M1)
    (NE: M1 !!c l ≠ ∅):
    (* basic na safe *)
    let otl := L1.(tv).(cur).(pln) !!w l in  𝓝1 !!w l ⊑ otl →
    (* read na safe *)
    (o = Plain → ∀ 𝑚', 𝑚' ∈ M1 → mloc 𝑚' = l → Some (mto 𝑚') ⊑ otl) →
    ∃ 𝑚 tr 𝓝2, 𝑚.(mloc) = l
     ∧ Wf 𝑚
     ∧ (∀ t': time, is_Some (M1 !! (l,t')) → t' ⊑ 𝑚.(mto))
     ∧ read_step L1 M1 tr 𝑚 o (mkLC (read_new_tview o l 𝑚.(mto) tr
                                  (from_option id ∅ 𝑚.(mbase).(mrel)) L1.(tv)) L1.(prm))
     ∧ drf_read 𝑚 o tr 𝓝1 L1.(tv) M1 𝓝2
     ∧ (* initialized *)
         ((∃ t m, M1 !! (l,t) = Some m ∧ (m.(mval) = AVal → Some t ⊏ otl)) →
            isval 𝑚.(mbase).(mval)).
  Proof.
    move => otl VLe PLN.
    destruct (gmap_top_nonempty (flip (⊑)) _ NE) as [tm [mm Eqmm]].
    have Lmm: M1 !! (l, tm) = Some mm.
    { rewrite memory_lookup_cell.
      eapply gmap_top_lookup, Eqmm; eauto with typeclass_instances. }
    have Ler: L1.(tv).(cur).(rlx) !!w l ⊑ Some tm
    by apply (tview_closed_max _ M1 _ _ mm).
    have ?: otl ⊑ Some tm. { rewrite -Ler. by apply timeNap_sqsubseteq, pln_rlx. }
    set tr  := if decide (Relaxed ⊑ o) then fresh_aread_id 𝓝1 l else fresh_nread_id 𝓝1 l.
    set 𝓝2 := if decide (Relaxed ⊑ o) then add_aread_id 𝓝1 l tr else add_nread_id 𝓝1 l tr.
    exists (mkMsg l tm mm), tr, 𝓝2.
    have MAX: ∀ (t' : time) m', M1 !! (l, t') = Some m' → t' ⊑ tm.
    { move => ???.
      apply (gmap_top_top _ _ _ _ Eqmm), elem_of_dom.
      eexists. by rewrite -memory_lookup_cell. }
    split; [done|]. split; first by eapply msg_elem_wf_pre.
    split; last split; last split.
    - move => ? [? ?]. by eapply MAX.
    - constructor; [|done..]. simpl. constructor; [done| |done].
      destruct mm.(mrel) as [Vmm|] eqn:EqVmm; [|done].
      eapply (msg_wf_loc_to (mkMsg l tm mm)); [by eapply msg_elem_wf_pre|done].
    - constructor; simpl; [|by case_decide..].
      econstructor; [eauto|]. case_decide; [by apply PLN|done].
    - move => [ti [mi [Eqmi Lti]]] /=. rewrite memory_lookup_cell in Eqmi.
      destruct mm.(mval) eqn:Hmv; [|exfalso; by apply (ALLOC tm mm)|done].
      have CM: cell_min (M1 !!c l) = Some (tm, mm).
      { apply (alloc_inv_min_alloc _ AINV). by rewrite -memory_lookup_cell. }
      have ?: ti ∈ dom (gset time) (M1 !!c l) by apply elem_of_dom; eexists.
      have ?: tm = ti.
      { apply : (anti_symm (⊑)).
        - by apply (gmap_top_top _ _ _ _ CM).
        - by eapply (gmap_top_top _ _ _ _ Eqmm). }
      subst ti. rewrite memory_lookup_cell in Lmm.
      rewrite Eqmi in Lmm. inversion Lmm. subst. specialize (Lti Hmv).
      edestruct (irreflexivity (⊏) otl). by eapply strict_transitive_r.
  Qed.

  Lemma read_progress c1 l o b
    (CLOSED: c1.(lc).(tv) ∈ c1.(gb).(mem)) (WFM: Wf c1.(gb).(mem))
    (AINV: alloc_inv c1.(gb).(mem)) (ALLOC: allocated l c1.(gb).(mem))
    (NE: c1.(gb).(mem) !!c l ≠ ∅):
    (* basic na safe *)
    let otl := c1.(lc).(tv).(cur).(pln) !!w l in
    c1.(gb).(na) !!w l ⊑ otl →
    (* read na safe *)
    (o = Plain → ∀ 𝑚', 𝑚' ∈ c1.(gb).(mem) → mloc 𝑚' = l → Some (mto 𝑚') ⊑ otl) →
    ∃ c2 v, program_step b c1 (Read l v o) c2
    ∧ (* initialized *)
      ((∃ t m, c1.(gb).(mem) !! (l,t) = Some m ∧ (m.(mval) = AVal → Some t ⊏ otl))
        → isval v).
  Proof.
    move => otl VLe PLN.
    destruct (read_step_progress _ _ _ _ _ CLOSED WFM AINV ALLOC NE VLe PLN)
      as [𝑚  [tr [𝓝2 [EQL [_ [_ [RS [DRF ISVAL]]]]]]]].
    exists (mkCFG (mkLC (read_new_tview o l 𝑚.(mto) tr
                                        (from_option id ∅ 𝑚.(mbase).(mrel))
                                        c1.(lc).(tv))
                        c1.(lc).(prm))
                  (mkGB c1.(gb).(sc) 𝓝2 c1.(gb).(mem))).
    exists 𝑚.(mbase).(mval). subst l. split; [by eapply (PStepR _ _ 𝑚)|done].
  Qed.

  (** Progress for writes *)
  Definition write_new_na (o : memOrder) l t 𝓝 : timeNap :=
    if decide (Relaxed ⊑ o) then 𝓝 else set_write_time 𝓝 l t.

  Definition write_new_mview o l t Vr 𝓥 : option view :=
    let V   := mkView_same {[l := t]} in
    let Vra := if decide (AcqRel ⊑ o) then 𝓥.(cur) ⊔ V else V in
    let V'  := from_option id ∅ (𝓥.(rel) !! l) ⊔ Vra in
      if decide (Relaxed ⊑ o) then Some (V' ⊔ 𝓥.(frel) ⊔ Vr) else None.

  Lemma write_new_mview_pln_time o l t rsa rsn Vr 𝓥:
        𝓥 .(cur).(pln) !!w l ⊑ Some t → Vr.(pln) !!w l ⊑ Some t →
    pln (from_option id ∅ (write_new_mview o l (t,rsa,rsn) Vr 𝓥)) !!w l ⊑ Some t.
  Proof.
    rewrite /write_new_mview => Le1 Le2. case_match; [|done]. simpl.
    rewrite 3!timeNap_lookup_w_join.
    apply lat_join_lub; [|done].
    apply lat_join_lub; [|rewrite -Le1; apply timeNap_sqsubseteq, frel_cur].
    have ?: pln (mkView_same {[l := (t, rsa,rsn)]}) !!w l ⊑ Some t.
    { rewrite (timeNap_lookup_w (pln (mkView_same {[l := (t,rsa,rsn)]})) l t rsa rsn); [done|].
      by rewrite /= lookup_insert. }
    apply lat_join_lub; [|case decide => ? //].
    - have Lel := rel_cur 𝓥 l. destruct (𝓥.(rel) !! l); [|done].
      etrans; [apply timeNap_sqsubseteq,Lel|done].
    - rewrite timeNap_lookup_w_join. by apply lat_join_lub.
  Qed.

  Lemma write_new_mview_ltView_wf o l t rsa rsn Vr 𝓥:
    𝓥 .(cur).(pln) !!w l ⊑ Some t → Vr.(pln) !!w l ⊑ Some t →
    Wf (mkLTV l t (write_new_mview o l (t,rsa,rsn) Vr 𝓥)).
  Proof.
    move => Le1 Le2 V /= LE. split.
    - rewrite (_ : V = default ∅ (Some V)); [|done]. rewrite -LE.
      by apply write_new_mview_pln_time.
    - move : LE. rewrite /write_new_mview. case_match; last done.
      have ?: Some t ⊑ ({[l := (t,rsa,rsn)]}: timeNap) !!w l
        by rewrite timeNap_lookup_w_insert.
      move => [<-].
      destruct (𝓥.(rel) !! l); case_match => /=;
      rewrite ?timeNap_lookup_w_join; solve_lat.
  Qed.

  Lemma write_new_mview_closed o l (t t' : time) v (Vr: view) 𝓥 M1 C V'
    (MAX : ∀ t' : time, is_Some (M1 !! (l, t')) → (t' < t)%Qc)
    (CLOSED: 𝓥 ∈ M1) (CLOSEDV: Vr ∈ M1):
    (write_new_mview o l (t,∅,∅) Vr 𝓥) ∈
      (<[l := (<[t := mkBMes (VAL:=VAL) v t' V']>C) ]>M1).
  Proof.
    rewrite /write_new_mview.
    set V:= mkView_same {[l := (t,∅,∅)]}.
    set M2: memory := <[l := (<[t := mkBMes (VAL:=VAL) v t' V' ]>C) ]>M1.
    have ?: {[l := (t, ∅, ∅)]} ∈ M2.
    { move => l1 t1 Eq1. apply timeNap_lookup_w_singleton_Some in Eq1 as [??].
      subst l1 t1. eexists. exists t.
      split; [done|by rewrite lookup_mem_first_eq lookup_insert]. }
    have ?: V ∈ M2 by constructor.
    have ?: 𝓥.(cur).(rlx) ∈ M2.
    { move => l1 t1.
      case (decide (l1 = l)) => [->|?] Eqt1.
      - rewrite /M2. setoid_rewrite lookup_mem_first_eq.
        eexists. exists t. rewrite lookup_insert. split; last done.
        apply CLOSED in Eqt1 as [m2 [t2 [Le2 Eqt2]]].
        etrans; first exact Le2. apply Qclt_le_weak, MAX. by eexists.
      - apply CLOSED in Eqt1 as [m2 [t2 Eqt2]].
        exists m2, t2. by rewrite (lookup_mem_first_ne l l1) //. }
    have ?: 𝓥.(cur) ∈ M2.
    { constructor; last done. by rewrite pln_rlx. }
    have ?: 𝓥.(frel) ∈ M2 by rewrite frel_cur.
    have ?: Vr ∈ M2.
    { apply closed_timenap_wf_view.
      move => l1 t1.
      case (decide (l1 = l)) => [->|?] Eqt1.
      - rewrite /M2. setoid_rewrite lookup_mem_first_eq.
        eexists. exists t. rewrite lookup_insert. split; last done.
        apply CLOSEDV in Eqt1 as [m2 [t2 [Le2 Eqt2]]].
        etrans; first exact Le2. apply Qclt_le_weak, MAX. by eexists.
      - apply CLOSEDV in Eqt1 as [m2 [t2 Eqt2]].
        exists m2, t2. by rewrite (lookup_mem_first_ne l l1) //. }
    case_match; last done. destruct (𝓥.(rel) !! l) as [V0|] eqn:EQV.
    - have CLOSED0: V0 ∈ M1.
      { change (Some V0 ∈ M1). rewrite -EQV. apply CLOSED. }
      have ?: V0.(rlx) ∈ M2.
      { move => l1 t1.
        case (decide (l1 = l)) => [->|?] Eqt1.
        - rewrite /M2. setoid_rewrite lookup_mem_first_eq.
          eexists. exists t. rewrite lookup_insert. split; last done.
          apply CLOSED0 in Eqt1 as [m2 [t2 [Le2 Eqt2]]].
          etrans; first exact Le2. apply Qclt_le_weak, MAX. by eexists.
        - apply CLOSED0 in Eqt1 as [m2 [t2 Eqt2]].
          exists m2, t2. by rewrite (lookup_mem_first_ne l l1). }
      have ?: V0 ∈ M2.
      { constructor; last done. by rewrite pln_rlx. }
      case_match; simpl; repeat apply join_closed_view=>//.
    - case_match; simpl; repeat apply join_closed_view=>//.
  Qed.

  Lemma memory_write_addins_progress
    𝓥 l o (t: time) v (Vr: view) (P1 M1: memory) m
    (SUB: ((⊆) : SubsetEq (gmap _ _)) P1 M1)
    (CLOSED: 𝓥 ∈ M1) (CLOSEDV: Vr ∈ M1)
    (ALLOC: allocated l M1) (Eqm: M1 !! (l, t) = Some m)
    (MAX: ∀ t': time, is_Some (M1 !! (l,t')) → t' ⊑ t):
    let VR := write_new_mview o l ((t+1)%Qp,∅,∅) Vr 𝓥 in
    let 𝑚 := mkMsg l (t+1)%Qp (mkBMes (VVal v) t VR) in
    ∃ P2 M2, memory_write (VAL:=VAL) P1 M1 𝑚 P2 M2 OpKindAddIns true.
  Proof.
    move => VR 𝑚.
    have DISJ: ∀ t0 m0, (M1 !!c l) !! t0 = Some m0 → (mfrom m0, t0] ## (t, (t + 1)%Qp].
    { move => t0 m0 Eq0 t1 [_ /= Le1] [/= /Qclt_nge Gt1 _].
      apply Gt1. etrans; [apply Le1|apply MAX]. rewrite memory_lookup_cell. eauto. }
    eexists. exists (<[l := (<[(t+1)%Qp := 𝑚.(mbase) ]>(M1 !!c l)) ]>M1).
    apply MemWrite with (P':=<[l := (<[(t+1)%Qp := 𝑚.(mbase) ]>(P1 !!c mloc 𝑚)) ]>P1).
    - constructor; [..|done|done|].
      + econstructor; first eauto. constructor => /= t0 m0 Eq0.
        apply DISJ. specialize (SUB (l, t0)). revert SUB.
        rewrite !memory_lookup_cell Eq0. case: lookup; [by inversion 1|done].
      + econstructor. by constructor.
      + have Le: ∀ V, V ∈ M1 → V.(pln) !!w l ⊑ Some (t + 1)%Qp.
        { move => V CV.
          destruct (V.(pln) !!w l) as [tv|] eqn: Eqtv; [|done].
          apply CV in Eqtv as [m' [to' [Le' Eq']]].
          change (tv ≤ (t + 1)%Qc)%Qc. etrans; [apply Le'|].
          etrans; [apply MAX; by eexists|].
          rewrite -{1}(Qcplus_0_r t). by apply Qcplus_le_compat. }
        constructor; [by apply Qp_lt_sum; exists 1%Qp|apply write_new_mview_ltView_wf].
        * apply Le, CLOSED.
        * by apply Le.
      + apply write_new_mview_closed; auto.
        move => ? /MAX Le. eapply Qcle_lt_trans; first exact Le.
        by apply Qp_lt_sum; exists 1%Qp.
      + exists t. split; first by eauto.
        rewrite -{1}(Qcplus_0_r t). by apply (Qcplus_le_compat).
    - econstructor. econstructor. by rewrite memory_cell_lookup_insert lookup_insert.
  Qed.

  Lemma write_step_addins_progress l o v t m (Vr: view) M1 L1 𝓝1
    (CLOSED: L1.(tv) ∈ M1)
    (SUB : ((⊆) : SubsetEq (gmap _ _)) L1.(prm) M1)
    (AINV: alloc_inv M1) (ALLOC: allocated l M1)
    (REL: StrongRelaxed ⊑ o → mem_nosync_loc L1.(prm) l)
    (Eqm: M1 !! (l, t) = Some m)
    (MAX: ∀ t': time, is_Some (M1 !! (l,t')) → t' ⊑ t)
    (CLOSEDV: Vr ∈ M1) :
    (* na write safe *)
    𝓝1 !!nr l ⊑ L1.(tv).(cur).(rlx) !!nr l →
    let ot := L1.(tv).(cur).(pln) in 𝓝1 !!w l ⊑ ot !!w l →
    (o = Plain → ∀ 𝑚', 𝑚' ∈ M1 → mloc 𝑚' = l → Some (mto 𝑚') ⊑ ot !!w l) →
    (o = Plain → 𝓝1 !!ar l ⊑ L1.(tv).(cur).(rlx) !!ar l) →
    let VR := write_new_mview o l ((t+1)%Qp,∅,∅) Vr L1.(tv) in
    let 𝑚 := mkMsg l (t+1)%Qp (mkBMes (VVal v) t VR) in
    ∃ L2 M2,
      write_step L1 M1 𝑚 o true Vr L2 M2
     ∧ drf_write 𝑚 o 𝓝1 L1.(tv) M1 (write_new_na o l (t + 1)%Qp 𝓝1).
  Proof.
    move => Vnar otl Vnaw Vna VAR VR 𝑚.
    destruct (memory_write_addins_progress _ _ o t v _ _ _ _
                                           SUB CLOSED CLOSEDV ALLOC Eqm MAX)
      as [P2 [M2 WRITE]].
    have Ler: L1.(tv).(cur).(rlx) !!w l ⊑ Some t.
    { apply (tview_closed_max _ M1 _ _ m); [done|].
      rewrite memory_lookup_cell in Eqm.
      apply gmap_top_inv; eauto with typeclass_instances.
      move => ? /elem_of_dom [??].
      apply MAX. rewrite memory_lookup_cell. by eexists. }
    eexists. exists M2. split.
    - econstructor; [split; by eauto|by eauto|..]. econstructor; eauto; simpl.
      eapply strict_transitive_r; first by eauto.
      by apply total_not_strict, Qclt_not_le, Qp_lt_sum; exists 1%Qp.
    - econstructor.
      + econstructor; [eauto|]. case_match; [|done]. by apply Vna.
      + by rewrite Vnar.
      + case_match; [|done]. by apply VAR.
      + rewrite /write_new_na. case_match; [done|split; [|done]].
        move => 𝑚'. rewrite /elem_of /message_ElemOf.
        move => Eq𝑚 EqL. rewrite EqL in Eq𝑚.
        eapply Qcle_lt_trans; last by apply Qp_lt_sum; exists 1%Qp.
        apply MAX. by eexists.
  Qed.

  Lemma write_addins_progress c1 l o v
    (CLOSED: c1.(lc).(tv) ∈ c1.(gb).(mem))
    (SUB : ((⊆) : SubsetEq (gmap _ _)) c1.(lc).(prm) c1.(gb).(mem))
    (AINV: alloc_inv c1.(gb).(mem)) (ALLOC: allocated l c1.(gb).(mem))
    (NEMP: ∃ t, is_Some (c1.(gb).(mem) !! (l,t)))
    (REL: StrongRelaxed ⊑ o → mem_nosync_loc c1.(lc).(prm) l):
    (* write na safe *)
    c1.(gb).(na) !!nr l ⊑ c1.(lc).(tv).(cur).(rlx) !!nr l →
    let ot := c1.(lc).(tv).(cur).(pln) in
    c1.(gb).(na) !!w l ⊑ ot !!w l →
    (o = Plain → ∀ 𝑚', 𝑚' ∈ c1.(gb).(mem) → mloc 𝑚' = l → Some (mto 𝑚') ⊑ ot !!w l) →
    (o = Plain → c1.(gb).(na) !!ar l ⊑ c1.(lc).(tv).(cur).(rlx) !!ar l) →
    ∃ c2, program_step true c1 (Write l v o) c2.
  Proof.
    move => Vler otl Vlew Vna VAR.
    destruct NEMP as [ts [ms Eqms]]. rewrite memory_lookup_cell in Eqms.
    destruct (gmap_top_nonempty_2 (flip (⊑)) _ _ _ Eqms) as [t [m Eqm]].
    assert (EqL := gmap_top_lookup _ _ _ _ Eqm). rewrite -memory_lookup_cell in EqL.
    set MAX:= gmap_top_top _ _ _ _ Eqm.
    destruct (write_step_addins_progress _ o v t _ ∅ _ _ c1.(gb).(na)
                                           CLOSED SUB AINV ALLOC REL EqL)
      as [L2 [M2 [WRITE DRF]]]; [..|done|done|done|done|done|].
    { move => t' [m' Eqt'].
      apply (gmap_top_top _ _ _ _ Eqm), (elem_of_dom (M:=gmap time)).
      rewrite -memory_lookup_cell. by eexists. }
    eexists. by eapply (PStepW _ c1 (mkMsg l _ (mkBMes (VVal v) t _))).
  Qed.

  Lemma read_step_stronger_read L (M: memory) tr 𝑚 or1 or2 :
    let L': memOrder → local :=
      λ o, (mkLC (read_new_tview o 𝑚.(mloc) 𝑚.(mto) tr
                        (from_option id ∅ 𝑚.(mbase).(mrel)) L.(tv)) L.(prm)) in
    Relaxed ⊑ or1 → read_step L M tr 𝑚 or1 (L' or1) → read_step L M tr 𝑚 or2 (L' or2).
  Proof.
    move => L' oLE. inversion 1; subst; simpl.
    constructor; [|done..].
    inversion READ. simpl in *. constructor; [done..|].
    move =>?. by apply RLX.
  Qed.

  Lemma drf_read_relaxed 𝑚 𝓝 𝓝' 𝓥 M tr or1 or2:
    Relaxed ⊑ or1 → Relaxed ⊑ or2 → drf_read 𝑚 or1 tr 𝓝 𝓥 M 𝓝' → drf_read 𝑚 or2 tr 𝓝 𝓥 M 𝓝'.
  Proof.
    move => oLE1 oLE2. inversion 1; subst. econstructor.
    - econstructor; eauto. rewrite decide_False; [|move => ?; by subst or2].
      inversion PRE. subst. rewrite decide_False in LAST; [done|].
      move => ?; by subst or1.
    - move: POSTG. by rewrite (decide_True _ _ oLE1) (decide_True _ _ oLE2).
    - move: POSTL. by rewrite (decide_True _ _ oLE1) (decide_True _ _ oLE2).
  Qed.

  Lemma read_step_relaxed L L' (M: memory) 𝑚 tr or1 or2:
    Relaxed ⊑ or1 → read_step L M tr 𝑚 or1 L' →
      ∃ L2, read_step L M tr 𝑚 or2 L2.
  Proof.
    move => oLE. inversion 1. subst; simpl in *.
    inversion READ. subst; simpl in *.
    eexists. constructor; [|done..].
    constructor; [done..|].
    move =>?. by apply RLX.
  Qed.

  (* We match updates with C/Rust CASes, which have success/failure modes,
    thus effectively correspond to 3 access modes: read failure mode orf,
    read success mode or, and write success mod ow.
    C11 requires that orf ⊑ or. This condition is removed in C17.
    Additionally, progress forbids plain CASes. *)
  Lemma update_read_write_addins_progress c1 l vr vw orf or ow
    (CLOSED: c1.(lc).(tv) ∈ c1.(gb).(mem))
    (SUB : ((⊆) : SubsetEq (gmap _ _)) c1.(lc).(prm) c1.(gb).(mem))
    (AINV: alloc_inv c1.(gb).(mem)) (WFM: Wf c1.(gb).(mem))
    (ALLOC: allocated l c1.(gb).(mem)) (NE: c1.(gb).(mem) !!c l ≠ ∅)
    (REL: StrongRelaxed ⊑ ow → mem_nosync_loc c1.(lc).(prm) l)
    (RLX: Relaxed ⊑ orf) (RLX1: Relaxed ⊑ or) (RLX2: Relaxed ⊑ ow) :
    (* basic na safe *)
    c1.(gb).(na) !!nr l ⊑ c1.(lc).(tv).(cur).(rlx) !!nr l →
    let ot := c1.(lc).(tv).(cur).(pln) in
    c1.(gb).(na) !!w l ⊑ ot !!w l →
    (* initialized *)
    (∃ t m, c1.(gb).(mem) !! (l,t) = Some m ∧ (m.(mval) = AVal → Some t ⊏ ot !!w l) ) →
    (∃ c2 v, v ≠ vr ∧ program_step true c1 (Read l (VVal v) orf) c2)
    ∨ (∃ c2, program_step true c1 (Update l vr vw or ow) c2).
  Proof.
    move => VLer otl VLe INIT.
    destruct (read_step_progress _ orf _ _ c1.(gb).(na) CLOSED WFM AINV ALLOC NE VLe)
      as [𝑚 [tr [𝓝2[EQL [WFm [MAx [RS [DRF ISVAL]]]]]]]];
      [move => ?; by subst orf|].
    specialize (ISVAL INIT).
    set L2 : memOrder → local :=
      λ o, mkLC (read_new_tview o l 𝑚.(mto) tr (from_option id ∅ 𝑚.(mbase).(mrel))
                                c1.(lc).(tv)) c1.(lc).(prm).
    inversion ISVAL as [vm Eqvm].
    case (decide (vm = vr)) => ?; last first.
    { left. exists (mkCFG (L2 orf) (mkGB c1.(gb).(sc) 𝓝2 c1.(gb).(mem))).
      exists vm. split; first done. subst l. rewrite Eqvm. by eapply (PStepR _ _ 𝑚). }
    subst vm l.
    have IN: 𝑚 ∈ c1.(gb).(mem) by inversion RS; inversion READ.
    have RS':= read_step_stronger_read _ _ _ _ _ or RLX RS.
    have LE':= read_step_tview_sqsubseteq _ _ _ _ _ _ RS'.
    have ?: 𝓝2 = add_aread_id c1.(gb).(na) 𝑚.(mloc) tr. { clear - DRF RLX. inversion DRF.
      subst. by rewrite decide_True in POSTG. } subst 𝓝2.
    destruct (write_step_addins_progress 𝑚.(mloc) ow vw 𝑚.(mto) 𝑚.(mbase)
                 (from_option id ∅ 𝑚.(mbase).(mrel))
                 c1.(gb).(mem) (L2 or) (add_aread_id c1.(gb).(na) 𝑚.(mloc) tr))
      as [L3 [M2 [WRITE DRFW]]]; auto.
    - by apply (read_step_closed_tview _ _ _ _ _ _ RS').
    - have ?:= mem_wf_closed _ WFM _ _ _ IN.
      by destruct 𝑚.(mbase).(mrel).
    - rewrite add_aread_id_nread VLer. by apply timeNap_sqsubseteq, LE'.
    - rewrite add_aread_id_write VLe. by apply timeNap_sqsubseteq, LE'.
    - move => ?. by subst.
    - move => ?. by subst.
    - right. eexists.
      have DRF':= drf_read_relaxed _ _ _ _ _ _ _ _ RLX RLX1 DRF.
      by eapply (PStepU _ _ _ (mkMsg _ (𝑚.(mto) + 1) (mkBMes _ 𝑚.(mto) _))); eauto.
  Qed.

  Lemma acq_fence_progress c1:
    ∃ c2, program_step true c1 (Fence AcqRel Relaxed) c2.
  Proof. eexists. constructor. constructor. Qed.

  Lemma rel_fence_progress 𝓥 σ:
    ∃ c2, program_step true (mkCFG (mkLC 𝓥 ∅) σ) (Fence Relaxed AcqRel) c2.
  Proof. eexists. constructor. constructor=>//. Qed.

  Lemma sc_fence_progress 𝓥 σ:
    ∃ c2, program_step true (mkCFG (mkLC 𝓥 ∅) σ) (Fence SeqCst SeqCst) c2.
  Proof. eexists. constructor. constructor=>//=. Qed.

  Lemma syscall_progress 𝓥 σ ev:
    ∃ c2, program_step true (mkCFG (mkLC 𝓥 ∅) σ) (SysCall ev) c2.
  Proof. eexists. constructor. constructor=>//=. Qed.
End Steps.
