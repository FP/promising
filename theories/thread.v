(* FIXME: This is necessary to get Qp_car as a coercion... *)
From stdpp Require Import numbers.
Require Import Coq.QArith.Qcanon.
From promising Require Export tview event.

Set Default Proof Using "Type".

Section Thread.

  Context `{LocFacts loc} `{CVAL: Countable VAL} `{Shift loc}
          `{!Allocator (state:=@memory loc _ VAL)}.

  Notation memory := (@memory _ _ VAL).
  Notation message := (@message _ _ VAL).
  Notation event := (@event loc VAL).

  Record local  := mkLC  { tv : threadView; prm : memory }.
  Record global := mkGB { sc: timeNap; na : timeNap; mem: memory }.
  Record config := mkCFG { lc: local; gb : global; }.

  Implicit Type (𝑚: message) (M: memory) (𝓝: timeNap)
                (L: local) (G: global) (c: config).

  Definition dealloc_na_agree M 𝓝 :=
    ∀ l t m, M !! (l, t) = Some m → m.(mval) = DVal → Some t ⊑ 𝓝 !!w l.

  Record local_le lc1 lc2 :=
    mkLocalSqSubsetEq {
      local_sqsubseteq_tv  : lc1.(tv)  ⊑ lc2.(tv)  ;
      local_sqsubseteq_prm : lc1.(prm)  ⊑ lc2.(prm) ;
    }.
  Global Instance local_sqsubseteq : SqSubsetEq local := local_le.

  Global Instance local_sqsubseteq_po :
    PartialOrder ((⊑) : SqSubsetEq local).
  Proof.
    constructor; [constructor|]; [done|..].
    - intros [][][] [??] [??]. constructor; by etrans.
    - intros [][] [??][??]. simpl in *.
      f_equal; by apply : (anti_symm (⊑)).
  Qed.

  Record global_wf' G := {
    global_wf_mem : Wf G.(mem);
    global_wf_alloc : alloc_inv G.(mem);
    global_wf_dealloc_na : dealloc_na_agree G.(mem) G.(na);
    global_wf_sc : G.(sc) ∈ G.(mem);
    global_wf_na : G.(na) ∈ G.(mem);
  }.
  Global Instance global_wf : Wellformed global := global_wf'.

  Record global_le g1 g2 :=
    mkGlobalSqSubsetEq {
      global_sqsubseteq_sc  : g1.(sc)  ⊑ g2.(sc)  ;
      global_sqsubseteq_na  : g1.(na)  = g2.(na) ; (* YES WE CAN *)
      global_sqsubseteq_mem : memory_le g1.(mem) g2.(mem)
    }.
  Global Instance global_sqsubseteq : SqSubsetEq global := global_le.

  Global Instance global_sqsubseteq_po :
    PartialOrder ((⊑) : SqSubsetEq global).
  Proof.
    constructor; [constructor|]; [done|..].
    - intros [][][] [???] [???]. constructor; intros; by etrans.
    - intros [][] [???][???]. simpl in *. f_equal; [|done|]; by apply : anti_symm.
  Qed.

  Record config_wf' c := {
    config_wf_global: Wf c.(gb);
    config_wf_closed_tview : c.(lc).(tv) ∈ c.(gb).(mem);
    config_wf_promise_mem : ((⊆) : SubsetEq (gmap _ _)) c.(lc).(prm) c.(gb).(mem);
  }.
  Global Instance config_wf : Wellformed config := config_wf'.

  Record config_le c1 c2 :=
    mkCFGSqSubsetEq {
      config_sqsubseteq_local  : c1.(lc)  ⊑ c2.(lc)  ;
      config_sqsubseteq_global : c1.(gb)  ⊑ c2.(gb) ;
    }.
  Global Instance config_sqsubseteq : SqSubsetEq config := config_le.

  Global Instance config_sqsubseteq_po :
    PartialOrder ((⊑) : SqSubsetEq config).
  Proof.
    split; [split|]; [done|..].
    - intros [][][] [??] [??]. split; by etrans.
    - intros [][] [??][??]. simpl in *. f_equal; by apply : anti_symm.
  Qed.

  (** Thread-local non-promising steps *)

  (* <<𝓥 ,P>,M> -{ R(l,v,o) }-> <<𝓥 ',P>,M> *)
  Inductive read_step L1 M1 tr 𝑚 o: local → Prop :=
    | ReadStep tview2
        (READ: read_helper L1.(tv) o 𝑚.(mloc) 𝑚.(mto) tr
                           (from_option id ∅ 𝑚.(mbase).(mrel)) tview2)
        (IN: 𝑚 ∈ M1)
        (ALLOC: allocated 𝑚.(mloc) M1)
    : read_step L1 M1 tr 𝑚 o (mkLC tview2 L1.(prm)).

  (* <<𝓥 ,P>,M> -{ W(l,v,o) }-> <<𝓥 ',P'>,M'> *)
  Inductive write_step L1 M1 𝑚  o: bool → view → local → memory → Prop :=
    | WriteStep 𝓥2 P2 M2 k b V
        (RELW: StrongRelaxed ⊑ o
               → mem_nosync_loc L1.(prm) 𝑚.(mloc) ∧ k = OpKindAddIns)
        (WRITE: memory_write L1.(prm) M1 𝑚 P2 M2 k b)
        (WVIEW : write_helper L1.(tv) o 𝑚.(mloc) 𝑚.(mto)
                              V 𝑚.(mbase).(mrel) 𝓥2)
    : write_step L1 M1 𝑚 o b V (mkLC 𝓥2 P2) M2.

  (* <<𝓥 ,P>,M> -{ U(l,vr,vw,or,ow) }-> <<𝓥 ',P'>,M'> *)
  (* Inductive update_step L1 M1 𝑚1 𝑚2 or ow: bool → local → memory → Prop :=
    | UpdateStep L2 L3 M3 b
        (READ: read_step L1 M1 𝑚1 or L2)
        (WRITE: write_step L2 M1 𝑚2 ow b (from_option id ∅ 𝑚1.(mbase).(mrel)) L3 M3)
        (ADJ: 𝑚1.(mto) = 𝑚2.(mbase).(mfrom))
        (SAME: 𝑚1.(mloc) = 𝑚2.(mloc))
    : update_step L1 M1 𝑚1 𝑚2 or ow b L3 M3. *)

  (* <𝓥 > -{ F_acq }-> <𝓥 '> *)
  Program Definition acq_fence_tview 𝓥 :=
    mkTView 𝓥.(rel) 𝓥.(frel) 𝓥.(acq) 𝓥.(acq) _ _ _ _.
  Next Obligation.
    intros. apply bool_decide_pack. etrans; [apply rel_dom|]. by rewrite cur_acq.
  Qed.
  Next Obligation. intros. apply bool_decide_pack=>l. by rewrite rel_cur cur_acq. Qed.
  Next Obligation. intros. apply bool_decide_pack. by rewrite frel_cur cur_acq. Qed.
  Next Obligation. intros. by apply bool_decide_pack. Qed.

  Inductive acq_fence_step 𝓥 : threadView → Prop :=
    | AcqFenceStep : acq_fence_step 𝓥 (acq_fence_tview 𝓥).

  (* <𝓥 ,P> -{ F_rel }-> <𝓥 ',P> *)
  Program Definition rel_fence_tview 𝓥 :=
    mkTView 𝓥.(rel) 𝓥.(cur) 𝓥.(cur) 𝓥.(acq) _ _ _ _.
  Next Obligation. intros. apply bool_decide_pack, rel_dom. Qed.
  Next Obligation. intros. apply bool_decide_pack, rel_cur. Qed.
  Next Obligation. intros. by apply bool_decide_pack. Qed.
  Next Obligation. intros. apply bool_decide_pack, cur_acq. Qed.
  Inductive rel_fence_step L1: local → Prop :=
    | RelFenceStep (RELEASE: mem_nosync L1.(prm))
    : rel_fence_step L1 (mkLC (rel_fence_tview L1.(tv)) L1.(prm)).

  (* <<𝓥 ,P>,𝓢> -{ F_sc }-> <<𝓥 ',P>,𝓢'> *)
  Inductive sc_fence_step L1 𝓢: local → timeNap → Prop :=
    | SCFenceStep 𝓥' 𝓢'
        (RELEASE: mem_nosync L1.(prm))
        (SC: sc_fence_helper L1.(tv) 𝓢 𝓥' 𝓢')
    : sc_fence_step L1 𝓢 (mkLC 𝓥' L1.(prm)) 𝓢'.

  (* <𝓥 ,M> -{ Alloc(l,n) }-> <𝓥 ',M'> *)
  Inductive alloc_step 𝓥1 M1 l n 𝑚s: threadView → memory → Prop :=
    | AllocStep M2 𝓥2
        (MEMALL: memory_alloc n l 𝑚s M1 M2)
        (VALL: alloc_helper 𝑚s 𝓥1 𝓥2)
    : alloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2.

  (* <𝓥 ,M> -{ Dealloc(l,n) }-> <𝓥 ',M'> *)
  Inductive dealloc_step 𝓥1 M1 l n: list message → threadView → memory → Prop :=
    | DeallocStep 𝑚s M2 𝓥2
        (MEMALL: memory_dealloc n l 𝑚s M1 M2)
        (VALL: alloc_helper 𝑚s 𝓥1 𝓥2)
    : dealloc_step 𝓥1 M1 l n 𝑚s 𝓥2 M2.

  (* DRF steps *)
  Definition fresh_aread_id (𝓝 : timeNap) l :=
    fresh (default ∅ (𝓝 !!ar l)).
  Definition fresh_nread_id (𝓝 : timeNap) l :=
    fresh (default ∅ (𝓝 !!nr l)).

  Definition add_aread_id (T : timeNap) l r :=
    partial_alter (λ o, (λ p, (p.1.1, {[r]} ∪ p.1.2, p.2)) <$> o) l T.
  Definition add_nread_id (T : timeNap) l r :=
    partial_alter (λ o, (λ p, (p.1, {[r]} ∪ p.2)) <$> o) l T.

  Definition set_write_time (T : timeNap) l t :=
    partial_alter (λ o, (λ p, (t, p.1.2, p.2)) <$> o) l T.

  Lemma add_aread_id_write T l r l' :
   add_aread_id T l r !!w l' = T !!w l'.
  Proof.
    rewrite /timeNap_lookup_write /add_aread_id.
    case: (decide (l' = l)) => [->|?].
    - rewrite lookup_partial_alter.
      by case: (_ !! _) => //.
    - rewrite lookup_partial_alter_ne //.
  Qed.

  Lemma add_nread_id_write T l r l' :
   add_nread_id T l r !!w l' = T !!w l'.
  Proof.
    rewrite /timeNap_lookup_write /add_nread_id.
    case: (decide (l' = l)) => [->|?].
    - rewrite lookup_partial_alter.
      by case: (_ !! _) => //.
    - rewrite lookup_partial_alter_ne //.
  Qed.

  Lemma add_aread_id_nread T l r l' :
   add_aread_id T l r !!nr l' = T !!nr l'.
  Proof.
    rewrite /timeNap_lookup_nread /add_aread_id.
    case: (decide (l' = l)) => [->|?].
    - rewrite lookup_partial_alter.
      by case: (_ !! _) => //.
    - rewrite lookup_partial_alter_ne //.
  Qed.

  Lemma add_nread_id_aread T l r l' :
   add_nread_id T l r !!ar l' = T !!ar l'.
  Proof.
    rewrite /timeNap_lookup_aread /add_nread_id.
    case: (decide (l' = l)) => [->|?].
    - rewrite lookup_partial_alter.
      by case: (_ !! _) => //.
    - rewrite lookup_partial_alter_ne //.
  Qed.

  Lemma add_aread_id_memory T l r M :
    T ∈ M → add_aread_id T l r ∈ M.
  Proof. move => IN l' ?. rewrite add_aread_id_write. by apply IN. Qed.

  Lemma add_nread_id_memory T l r M :
    T ∈ M → add_nread_id T l r ∈ M.
  Proof. move => IN l' ?. rewrite add_nread_id_write. by apply IN. Qed.

  Lemma add_aread_id_sqsubseteq T l r :
    T ⊑ add_aread_id T l r.
  Proof.
    intros l'. rewrite /add_aread_id.
    case: (decide (l' = l)).
    - move => -> {l'}. rewrite lookup_partial_alter.
      move: (T !! l) => [/=[[??] ?]|//].
      split; [|done]; split; [done|set_solver].
    - move => ?. rewrite lookup_partial_alter_ne //.
  Qed.

  Lemma add_nread_id_sqsubseteq T l r :
    T ⊑ add_nread_id T l r.
  Proof.
    intros l'. rewrite /add_aread_id.
    case: (decide (l' = l)).
    - move => -> {l'}. rewrite lookup_partial_alter.
      move: (T !! l) => [/=[[??] ?]|//].
      split; [done|set_solver].
    - move => ?. rewrite lookup_partial_alter_ne //.
  Qed.

  Lemma add_aread_id_mono T1 T2 l r:
    T1 ⊑ T2 → add_aread_id T1 l r ⊑ add_aread_id T2 l r.
  Proof.
    move => LE l'. apply timeNap_sqsubseteq. split; last split.
    - rewrite 2!add_aread_id_write. by apply timeNap_sqsubseteq.
    - rewrite /add_aread_id /= /timeNap_lookup_aread /=.
      case (decide (l' = l)) => [->|?].
      + rewrite !lookup_partial_alter. apply fmap_sqsubseteq; [apply _|].
        apply fmap_sqsubseteq; [|apply LE]. solve_proper.
      + do 2 (rewrite lookup_partial_alter_ne; [|done]).
        apply fmap_sqsubseteq; [apply _|apply LE].
    - rewrite 2!add_aread_id_nread. by apply timeNap_sqsubseteq.
  Qed.

  Lemma add_nread_id_mono T1 T2 l r:
    T1 ⊑ T2 → add_nread_id T1 l r ⊑ add_nread_id T2 l r.
  Proof.
    move => LE l'. apply timeNap_sqsubseteq. split; last split.
    - rewrite 2!add_nread_id_write. by apply timeNap_sqsubseteq.
    - rewrite 2!add_nread_id_aread. by apply timeNap_sqsubseteq.
    - rewrite /add_nread_id /= /timeNap_lookup_nread /=.
      case (decide (l' = l)) => [->|?].
      + rewrite !lookup_partial_alter. apply fmap_sqsubseteq; [apply _|].
        apply fmap_sqsubseteq; [|apply LE]. solve_proper.
      + do 2 (rewrite lookup_partial_alter_ne; [|done]).
        apply fmap_sqsubseteq; [apply _|apply LE].
  Qed.

  Lemma add_nread_id_dealloc_agree M T l t:
    dealloc_na_agree M T → dealloc_na_agree M (add_nread_id T l t).
  Proof. move => DA ???. rewrite add_nread_id_write. by apply DA. Qed.

  Lemma add_aread_id_dealloc_agree M T l t:
    dealloc_na_agree M T → dealloc_na_agree M (add_aread_id T l t).
  Proof. move => DA ???. rewrite add_aread_id_write. by apply DA. Qed.

  Lemma set_write_time_id T l t (HL: T !!w l = Some t):
    set_write_time T l t = T.
  Proof.
    apply (map_eq _ T) => l'. rewrite /set_write_time.
    case: (decide (l' = l)).
    - move => -> {l'}. rewrite lookup_partial_alter.
      destruct (T !! l) as [[[]]|] eqn:EqT; rewrite EqT; [|done]. simpl.
      f_equal. f_equal. rewrite (timeNap_lookup_w _ _ _ _ _ EqT) in HL.
      by inversion HL.
    - move => ?. rewrite lookup_partial_alter_ne //.
  Qed.

  Lemma mem_cut_insert_set_write M V l C t (IS: is_Some (V !! l)):
    <[l:=cell_cut t C]> (mem_cut M V) = mem_cut (<[l:=C]> M) (set_write_time V l t).
  Proof.
    rewrite /set_write_time
      (mem_cut_insert _ _ _ _ _ (default ∅ (V !!ar l)) (default ∅ (V !!nr l))).
    f_equal. apply (map_eq (<[_ := _]> V)) => l'.
    case (decide (l' = l)) => ?; [subst l'|].
    - rewrite lookup_insert lookup_partial_alter /=.
      destruct (V !! l) as [[[]]|] eqn:Eql; rewrite Eql; simpl.
      + by rewrite
          (timeNap_lookup_ar _ _ _ _ _ Eql) (timeNap_lookup_nr _ _ _ _ _ Eql) /=.
      + by destruct IS.
    - rewrite lookup_insert_ne; [|done].
      by rewrite lookup_partial_alter_ne; [|done].
  Qed.

  Lemma mem_cut_add_aread_id M V l t:
    mem_cut M (add_aread_id V l t) = mem_cut M V.
  Proof.
    rewrite /mem_cut /mem_cut_filter. apply (map_filter_equiv_eq (M:= gmap (loc * Qp))).
    move => [[l' t'] m'] /=. by rewrite add_aread_id_write.
  Qed.

  Lemma mem_cut_add_nread_id M V l t:
    mem_cut M (add_nread_id V l t) = mem_cut M V.
  Proof.
    rewrite /mem_cut /mem_cut_filter. apply (map_filter_equiv_eq (M:= gmap (loc * Qp))).
    move => [[l' t'] m'] /=. by rewrite add_nread_id_write.
  Qed.

  Section InsertAReadID.
    (* Context `{LocFacts loc}. *)
    Context (𝓥 : threadView) l r
            (pln' := 𝓥.(cur).(pln))
            (rlx' := add_aread_id 𝓥.(cur).(rlx) l r).

    Program Definition tview_insert_aread_id :=
      let cur' := {|pln := pln'; rlx := rlx'|} in
      let acq' := 𝓥.(acq) ⊔ cur' in
      mkTView 𝓥.(rel) 𝓥.(frel) cur' acq' _ _ _ _.
    Next Obligation.
      apply bool_decide_pack.
      etrans; last apply add_aread_id_sqsubseteq. eapply bool_decide_unpack, pln_rlx_dec.
    Qed.
    Next Obligation.
      intros. apply bool_decide_pack. pose proof (rel_dom 𝓥).
      rewrite /pln'/rlx' /=. rewrite /add_aread_id. etrans; [done|].
      move => l' /elem_of_dom [[??] ?]. apply/elem_of_dom.
      case: (decide (l' = l)) => ?.
      - simplify_eq. rewrite lookup_partial_alter.
        eexists. rewrite fmap_Some. eexists; split; [eassumption|reflexivity]. 
      - rewrite lookup_partial_alter_ne //. by eexists.
    Qed.
    Next Obligation.
      intros. apply bool_decide_pack=>l'.
      transitivity (Some 𝓥.(cur)).
      { generalize l'. eapply bool_decide_unpack, rel_cur_dec. }
      split; cbn; [done|]. rewrite /rlx'. apply add_aread_id_sqsubseteq.
    Qed.
    Next Obligation.
      intros. apply bool_decide_pack.
      transitivity (𝓥.(cur)).
      { eapply bool_decide_unpack, frel_cur_dec. }
      split; cbn; [done|]. apply add_aread_id_sqsubseteq.
    Qed.
    Next Obligation. intros. solve_lat. Qed.
  End InsertAReadID.

  Section InsertNReadID.
    (* Context `{LocFacts loc}. *)
    Context (𝓥 : threadView) l r
            (pln' := 𝓥.(cur).(pln))
            (rlx' := add_nread_id 𝓥.(cur).(rlx) l r).

    Program Definition tview_insert_nread_id :=
      let cur' := {|pln := pln'; rlx := rlx'|} in
      let acq' := 𝓥.(acq) ⊔ cur' in
      mkTView 𝓥.(rel) 𝓥.(frel) cur' acq' _ _ _ _.
    Next Obligation.
      apply bool_decide_pack.
      etrans; last apply add_nread_id_sqsubseteq. eapply bool_decide_unpack, pln_rlx_dec.
    Qed.
    Next Obligation.
      intros. apply bool_decide_pack. pose proof (rel_dom 𝓥).
      rewrite /pln'/rlx' /=. rewrite /add_aread_id. etrans; [done|].
      move => l' /elem_of_dom [[??] ?]. apply/elem_of_dom.
      case: (decide (l' = l)) => ?.
      - simplify_eq. rewrite lookup_partial_alter.
        eexists. rewrite fmap_Some. eexists; split; [eassumption|reflexivity]. 
      - rewrite lookup_partial_alter_ne //. by eexists.
    Qed.
    Next Obligation.
      intros. apply bool_decide_pack=>l'.
      transitivity (Some 𝓥.(cur)).
      { generalize l'. eapply bool_decide_unpack, rel_cur_dec. }
      split; cbn; [done|]. rewrite /rlx'. apply add_nread_id_sqsubseteq.
    Qed.
    Next Obligation.
      intros. apply bool_decide_pack.
      transitivity (𝓥.(cur)).
      { eapply bool_decide_unpack, frel_cur_dec. }
      split; cbn; [done|]. apply add_nread_id_sqsubseteq.
    Qed.
    Next Obligation. intros. solve_lat. Qed.
  End InsertNReadID.

  Inductive drf_pre l (𝓝 : timeNap) 𝓥 M (o : memOrder) : Prop :=
  | DRFLocalAT otl
        (OTL: 𝓥.(cur).(pln) !!w l = otl)
        (LAST: if decide (o = Plain) then
                 ∀ 𝑚', 𝑚' ∈ M → 𝑚'.(mloc) = l → Some (𝑚'.(mto)) ⊑ otl
               else 𝓝 !!w l ⊑ otl)
    : drf_pre l 𝓝 𝓥 M o
  .

  Inductive drf_read 𝑚 o tr 𝓝 𝓥 M : timeNap -> Prop :=
    | ReadDRF 𝓝'
        (PRE: drf_pre 𝑚.(mloc) 𝓝 𝓥 M o)
        (POSTG: if decide (Relaxed ⊑ o)
                then 𝓝' = add_aread_id 𝓝 𝑚.(mloc) tr
                else 𝓝' = add_nread_id 𝓝 𝑚.(mloc) tr)
      (* <[𝑚.(mloc) := 𝑚.(mto)]> 𝓝 *)
        (POSTL: if decide (Relaxed ⊑ o)
                then tr = fresh_aread_id 𝓝 𝑚.(mloc)
                else tr = fresh_nread_id 𝓝 𝑚.(mloc))
    : drf_read 𝑚 o tr 𝓝 𝓥 M 𝓝'.

  Inductive drf_write 𝑚 o 𝓝 𝓥 M : timeNap → Prop :=
    | WriteDRF 𝓝'
        (PREW: drf_pre 𝑚.(mloc) 𝓝 𝓥 M o)
        (PRER: 𝓝 !!nr 𝑚.(mloc) ⊑ 𝓥.(cur).(rlx) !!nr 𝑚.(mloc))
        (PRENR: if decide (o = Plain) then 𝓝 !!ar 𝑚.(mloc) ⊑ 𝓥.(cur).(rlx) !!ar 𝑚.(mloc) else True)
        (POSTG: if decide (Relaxed ⊑ o)
                then 𝓝' = 𝓝
                else
                  (∀ 𝑚', 𝑚' ∈ M → 𝑚'.(mloc) = 𝑚.(mloc) → 𝑚'.(mto) < 𝑚.(mto))%Qc
                  ∧ 𝓝' = set_write_time 𝓝 𝑚.(mloc) 𝑚.(mto)
        )
    : drf_write 𝑚 o 𝓝 𝓥 M 𝓝'.

  Inductive drf_update 𝑚1 𝑚2 o1 o2 tr 𝓝 𝓥 M : timeNap → threadView → Prop :=
    | UpdateDRF 𝓝' 𝓥'
        (READ: drf_read 𝑚1 o1 tr 𝓝 𝓥 M 𝓝')
        (WRITE: drf_write 𝑚2 o2 𝓝 𝓥 M 𝓝')
    : drf_update 𝑚1 𝑚2 o1 o2 tr 𝓝 𝓥 M 𝓝' 𝓥'.

  Inductive drf_dealloc 𝓥 M (𝑚s: list message) 𝓝 : Prop :=
  | DeallocDRF
      (PRE: ∀ 𝑚, 𝑚 ∈ 𝑚s → drf_pre 𝑚.(mloc) 𝓝 𝓥 M Plain)
      (PREAR: ∀ 𝑚, 𝑚 ∈ 𝑚s → 𝓝 !!ar 𝑚.(mloc) ⊑ 𝓥.(cur).(rlx) !!ar 𝑚.(mloc))
      (PRENR: ∀ 𝑚, 𝑚 ∈ 𝑚s → 𝓝 !!nr 𝑚.(mloc) ⊑ 𝓥.(cur).(rlx) !!nr 𝑚.(mloc))
      (* (POSTG: 𝓝' = foldr (λ 𝑚 𝓝'', set_write_time 𝓝'' 𝑚.(mloc) 𝑚.(mto)) 𝓝 𝑚s) *)
  : drf_dealloc 𝓥 M 𝑚s 𝓝.

  Inductive program_step b c1 : event → config → Prop :=
    (* ALLOC *)
    | PStepA l n 𝓥2 M2 𝑚s
          (ALLOC: alloc_step c1.(lc).(tv) c1.(gb).(mem) l (Pos.to_nat n) 𝑚s 𝓥2 M2)
        : program_step b c1
                (Alloc l n)
                (mkCFG (mkLC 𝓥2 c1.(lc).(prm))
                       (mkGB c1.(gb).(sc) (alloc_new_na c1.(gb).(na) 𝑚s) M2))
    (* DEALLOC *)
    | PStepD l n 𝑚s 𝓥2 M2
          (DEALLOC: dealloc_step c1.(lc).(tv) c1.(gb).(mem) l (Pos.to_nat n) 𝑚s 𝓥2 M2)
          (DRF: drf_dealloc c1.(lc).(tv) c1.(gb).(mem) 𝑚s c1.(gb).(na))
        : program_step b c1
                (Dealloc l n)
                (mkCFG (mkLC 𝓥2 c1.(lc).(prm))
                       (mkGB c1.(gb).(sc) (alloc_new_na c1.(gb).(na) 𝑚s) M2))
    (* READ *)
    | PStepR 𝑚 o L2 𝓝2 tr
          (READ: read_step c1.(lc) c1.(gb).(mem) tr 𝑚 o L2)
          (DRF: drf_read 𝑚 o tr c1.(gb).(na) c1.(lc).(tv) c1.(gb).(mem) 𝓝2)
        : program_step b c1
                (Read 𝑚.(mloc) 𝑚.(mbase).(mval) o)
                (mkCFG L2 (mkGB c1.(gb).(sc) 𝓝2 c1.(gb).(mem)))
    (* WRITE *)
    | PStepW 𝑚 o L2 M2 𝓝2 v
          (ISVAL: 𝑚.(mbase).(mval) = VVal v)
          (WRITE: write_step c1.(lc) c1.(gb).(mem) 𝑚 o b ∅ L2 M2)
          (DRF: drf_write 𝑚 o c1.(gb).(na) c1.(lc).(tv) c1.(gb).(mem) 𝓝2)
        : program_step b c1
                (Write 𝑚.(mloc) v o)
                (mkCFG L2 (mkGB c1.(gb).(sc) 𝓝2 M2))
    (* UPDATE *)
    | PStepU 𝑚1 𝑚2 or ow L2 L3 M3 tr 𝓝2 𝓝3 v1 v2
          (ISV1 : 𝑚1.(mbase).(mval) = VVal v1)
          (ISV2 : 𝑚2.(mbase).(mval) = VVal v2)
          (ADJ: 𝑚1.(mto) = 𝑚2.(mbase).(mfrom))
          (SAME: 𝑚1.(mloc) = 𝑚2.(mloc))
          (READ: read_step c1.(lc) c1.(gb).(mem) tr 𝑚1 or L2)
          (WRITE: write_step L2 c1.(gb).(mem) 𝑚2 ow b (from_option id ∅ 𝑚1.(mbase).(mrel)) L3 M3)
          (DRFR: drf_read 𝑚1 or tr c1.(gb).(na) c1.(lc).(tv) c1.(gb).(mem) 𝓝2)
          (DRFW: drf_write 𝑚2 ow 𝓝2 L2.(tv) c1.(gb).(mem) 𝓝3)
        :  program_step b c1
                (Update 𝑚1.(mloc) v1 v2 or ow)
                (mkCFG L3 (mkGB c1.(gb).(sc) 𝓝3 M3))
    (* ACQ-FENCE *)
    | PStepFAcq 𝓥'
          (FACQ: acq_fence_step c1.(lc).(tv) 𝓥')
        : program_step b c1
                (Fence AcqRel Relaxed) (mkCFG (mkLC 𝓥' c1.(lc).(prm)) c1.(gb))
    (* REL-FENCE *)
    | PStepFRel L2
          (FREL: rel_fence_step c1.(lc) L2)
        : program_step b c1 (Fence Relaxed AcqRel) (mkCFG L2 c1.(gb))
    (* SC-FENCE *)
    | PStepFSC L2 𝓢2
          (FSC: sc_fence_step c1.(lc) c1.(gb).(sc) L2 𝓢2)
        : program_step b c1
                (Fence SeqCst SeqCst)
                (mkCFG L2 (mkGB 𝓢2 c1.(gb).(na) c1.(gb).(mem)))
    (* SYSTEM CALL *)
    | PStepSys ev L2 𝓢2
          (FSC: sc_fence_step c1.(lc) c1.(gb).(sc) L2 𝓢2)
        : program_step b c1
                (SysCall ev)
                (mkCFG L2 (mkGB 𝓢2 c1.(gb).(na) c1.(gb).(mem)))
    (* SILENT *)
    | PStepSilent
        : program_step b c1 Silent c1
    .

  (** Promise step *)

  (* <<𝓥 ,P>, M> --> <<𝓥 ,P'>,M'> *)
  Inductive promise_step b 𝑚 k c1: config → Prop :=
    (* PROMISE *)
    | PromiseStep P2 M2
        (PROMISE: promise c1.(lc).(prm) c1.(gb).(mem) 𝑚 P2 M2 k b)
    : promise_step b 𝑚 k c1 (mkCFG (mkLC c1.(lc).(tv) P2) (mkGB c1.(gb).(sc) c1.(gb).(na) M2))
    .

  (** Thread step *)
  Inductive threadEvent :=
    | PromiseEvent (𝑚 : message) (k: @memoryOpKind _ _ VAL)
    | ProgramEvent (ev: event).

  Inductive thread_step
    : bool → bool → config → threadEvent → config → Prop :=
    | TStepPromise pf c1 𝑚 k c2
        (PROMISE: promise_step pf 𝑚 k c1 c2)
        : thread_step true pf c1 (PromiseEvent 𝑚 k) c2
    | TStepProgram pr pf c1 ev c2
        (PROGRAM: program_step pf c1 ev c2)
        : thread_step pr pf c1 (ProgramEvent ev) c2
    .

  Definition getEvent (ev : threadEvent): option sysEvent :=
    match ev with
    | ProgramEvent (SysCall e) => Some e
    | _ => None
    end.

  Inductive tau_pf_step
    (step : bool → bool → config → threadEvent → config → Prop) c1 c2
    : Prop :=
    | tauStep ev pf
        (STEP: step false pf c1 ev c2)
        (TAU: getEvent ev = None) : tau_pf_step step c1 c2.

  Definition consistent c :=
    ∀ 𝓢f Mf,
        future c.(gb).(mem) Mf
      → c.(gb).(sc) ⊑ 𝓢f
      → let cf := (mkCFG c.(lc) (mkGB 𝓢f c.(gb).(na) Mf))
        in Wf cf
      → ∃ c', tau_pf_step thread_step cf c' ∧ c'.(lc).(prm) = ∅.
End Thread.

Section Machine.
  (** Machine instantiations *)
  Context `{Countable VAL}.

  (** Thread steps for machine whose locations are positives *)
  Definition pos_program_step := program_step (loc:= positive) (VAL:=VAL).
  Definition pos_promise_step := promise_step (loc:= positive) (VAL:=VAL).
  Definition pos_thread_step := thread_step (loc:= positive) (VAL:=VAL).
  Definition pos_consistent := consistent (loc:= positive) (VAL:=VAL).

  (** Thread steps for machine whose locations are block+offset's *)
  Definition lbl_program_step := program_step (loc:= lblock) (VAL:=VAL).
  Definition lbl_promise_step := promise_step (loc:= lblock) (VAL:=VAL).
  Definition lbl_thread_step := thread_step (loc:= lblock) (VAL:=VAL).
  Definition lbl_consistent := consistent (loc:= lblock) (VAL:=VAL).
End Machine.
