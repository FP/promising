From stdpp Require Export numbers.
From Coq.QArith Require Import Qcanon.
From promising Require Export base.

Notation time := Qp (only parsing).
Notation time_Lat := Qp_Lat (only parsing).

Record interval := Interval { low : time; high : time }.
Notation "( low ,  high ]" := {| low := low; high := high |}.

(*FIXME??: i = (0,0) *)
Instance interval_wf : Wellformed interval := λ i, low i < high i.

Implicit Type (t : time) (i: interval).

Instance interval_elemof : ElemOf time interval
  := λ t i, low i < t ∧ t ≤ high i.

Instance interval_elem_dec t i : Decision (t ∈ i).
Proof.
  destruct i as [lb ub].
  case (Qclt_le_dec lb t) => [Lt|Le].
  - case (Qclt_le_dec ub t) => [Lt2|?]; [right|by left].
    move => [_ ?]. by apply (Qclt_not_le _ _ Lt2).
  - right. move => [ ? _]. by apply (Qcle_not_lt _ _ Le).
Qed.

Lemma interval_elem_ub (lb ub: time) (Lt: Wf (lb, ub]): ub ∈ (lb, ub].
Proof. done. Qed.

Instance interval_sqsubseteq : SqSubsetEq interval :=
  λ il ir, low ir ⊑ low il ∧ high il ⊑ high ir.

Lemma interval_sqsubseteq_in (il ir : interval) t:
  il ⊑ ir → t ∈ il → t ∈ ir.
Proof. move => [??] [??].
  split.
  - by eapply Qcle_lt_trans with (low il).
  - by etrans.
Qed.

Instance interval_disjoint : Disjoint interval
  := λ i1 i2, ∀ t, t ∈ i1 →  ¬ t ∈ i2.

Instance interval_disjoint_symm : @Symmetric interval (##).
Proof. move => ?? H ???. by eapply H. Qed.

Lemma adjacent_interval_disj t1 t2 t3: (t1, t2] ## (t2, t3].
Proof. move => t [_ /Qclt_not_le Le] [// ?]. Qed.

Lemma interval_sqsubseteq_disj i1 i2 i3: i2 ## i3 → i1 ⊑ i2 → i1 ## i3.
Proof.
  move => Disj ? ? ? ?.
  eapply Disj; eauto. by eapply interval_sqsubseteq_in.
Qed.

Lemma interval_disjoint_ub_neq l1 u1 l2 u2
  (DISJ : (l1, u1] ## (l2, u2])
  (WF1: Wf (l1, u1]) (WF2: Wf (l2, u2]):
  u1 ≠ u2.
Proof.
  move => Eq. rewrite Eq in DISJ. rewrite Eq in WF1.
  apply (DISJ u2); apply interval_elem_ub; [apply WF1|apply WF2].
Qed.
