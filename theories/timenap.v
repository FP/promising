From promising Require Export time location.
Set Default Proof Using "Type".

Notation read_id := (positive).
Notation read_ids := (gset positive).

Instance read_ids_Countable : Countable read_ids := _.

Section TimeNap.
  Context `{LocFacts loc}.

  Definition timeNap := gmap loc (time * read_ids * read_ids).

  Definition timeNap_lookup_write: Lookup loc time timeNap
    := fun l tn => fmap (fst ∘ fst) (tn !! l).
  Definition timeNap_lookup_aread: Lookup loc read_ids timeNap
    := fun l tn => fmap (snd ∘ fst) (tn !! l).
  Definition timeNap_lookup_nread: Lookup loc read_ids timeNap
    := fun l tn => fmap snd (tn !! l).
  Notation "m !!w i" := (timeNap_lookup_write i m) (at level 20) : stdpp_scope.
  Notation "m !!ar i" := (timeNap_lookup_aread i m) (at level 20) : stdpp_scope.
  Notation "m !!nr i" := (timeNap_lookup_nread i m) (at level 20) : stdpp_scope.

  Lemma timeNap_lookup_w' (T : timeNap) (l : loc) o:
    T !! l = o -> T !!w l = (fst ∘ fst) <$> o.
  Proof. destruct o; [rewrite fmap_Some|rewrite fmap_None]; eauto. Qed.

  Lemma timeNap_lookup_w (T : timeNap) (l : loc) (t : time) (rsa rsn : read_ids) :
    T !! l = Some (t,rsa,rsn) -> T !!w l = Some (t).
  Proof. rewrite fmap_Some. eauto. Qed.

  Lemma timeNap_lookup_ar' (T : timeNap) (l : loc) o:
    T !! l = o -> T !!ar l = (snd ∘ fst) <$> o.
  Proof. destruct o; [rewrite fmap_Some|rewrite fmap_None]; eauto. Qed.

  Lemma timeNap_lookup_ar (T : timeNap) (l : loc) t rsa rsn :
    T !! l = Some (t,rsa,rsn) -> T !!ar l = Some (rsa).
  Proof. rewrite fmap_Some. eauto. Qed.

  Lemma timeNap_lookup_nr' (T : timeNap) (l : loc) o:
    T !! l = o -> T !!nr l = snd <$> o.
  Proof. destruct o; [rewrite fmap_Some|rewrite fmap_None]; eauto. Qed.

  Lemma timeNap_lookup_nr (T : timeNap) (l : loc) t rsa rsn :
    T !! l = Some (t,rsa,rsn) -> T !!nr l = Some (rsn).
  Proof. rewrite fmap_Some. eauto. Qed.

  Lemma timeNap_lookup_wp (T : timeNap) (l : loc) p :
    T !! l = Some p -> T !!w l = Some p.1.1.
  Proof. rewrite fmap_Some. eauto. Qed.

  Lemma timeNap_lookup_arp (T : timeNap) (l : loc) p :
    T !! l = Some p -> T !!ar l = Some p.1.2.
  Proof. rewrite fmap_Some. eauto. Qed.

  Lemma timeNap_lookup_nrp (T : timeNap) (l : loc) p :
    T !! l = Some p -> T !!nr l = Some p.2.
  Proof. rewrite fmap_Some. eauto. Qed.

  Lemma timeNap_lookup_of_wp (T : timeNap) (l : loc) (t : time) :
    T !!w l = Some t -> ∃ p, p.1.1 = t ∧ T !! l = Some p.
  Proof. rewrite fmap_Some. move => [? [-> ->]]. eauto. Qed.

  Lemma timeNap_lookup_of_arp (T : timeNap) (l : loc) (rs : read_ids) :
    T !!ar l = Some rs -> ∃ p, p.1.2 = rs ∧ T !! l = Some p.
  Proof. rewrite fmap_Some. move => [? [-> ->]]. eauto. Qed.

  Lemma timeNap_lookup_of_nrp (T : timeNap) (l : loc) (rs : read_ids) :
    T !!nr l = Some rs -> ∃ p, p.2 = rs ∧ T !! l = Some p.
  Proof. rewrite fmap_Some. move => [? [-> ->]]. eauto. Qed.

  Lemma timeNap_lookup_of (T : timeNap) (l : loc) t rsa rsn:
    T !!w l = Some t -> T !!ar l = Some rsa -> T !!nr l = Some rsn
    -> T !! l = Some (t,rsa,rsn).
  Proof.
    move/(timeNap_lookup_of_wp _ _ _) => [[[??]?] /= [-> Eq]].
    move/(timeNap_lookup_of_arp _ _ _) => [[[??]?] /= [-> ?]].
    move/(timeNap_lookup_of_nrp _ _ _) => [[[??]?] /= [-> ?]].
    by simplify_map_eq.
  Qed.

  Lemma timeNap_lookup_w_join T1 T2 l:
    (T1 ⊔ T2) !!w l = T1 !!w l ⊔ T2 !!w l.
  Proof.
    rewrite /timeNap_lookup_write /= lookup_join.
    destruct (T1 !! l) as [[]|] eqn:Eq1; destruct (T2 !! l) as [[]|] eqn:Eq2;
      rewrite Eq1 Eq2; simpl; done.
  Qed.

  Lemma timeNap_lookup_ar_join T1 T2 l:
    (T1 ⊔ T2) !!ar l = T1 !!ar l ⊔ T2 !!ar l.
  Proof.
    rewrite /timeNap_lookup_aread /= lookup_join.
    destruct (T1 !! l) as [[]|] eqn:Eq1; destruct (T2 !! l) as [[]|] eqn:Eq2;
      rewrite Eq1 Eq2; simpl; done.
  Qed.

  Lemma timeNap_lookup_nr_join T1 T2 l:
    (T1 ⊔ T2) !!nr l = T1 !!nr l ⊔ T2 !!nr l.
  Proof.
    rewrite /timeNap_lookup_nread /= lookup_join.
    destruct (T1 !! l) as [[]|] eqn:Eq1; destruct (T2 !! l) as [[]|] eqn:Eq2;
      rewrite Eq1 Eq2; simpl; done.
  Qed.

  Lemma timeNap_lookup_w_insert T l p :
    <[l := p]> T !!w l = Some p.1.1.
  Proof. by rewrite /timeNap_lookup_write lookup_insert. Qed.

  Lemma timeNap_lookup_ar_insert T l p :
    <[l := p]> T !!ar l = Some p.1.2.
  Proof. by rewrite /timeNap_lookup_aread lookup_insert. Qed.

  Lemma timeNap_lookup_nr_insert T l p :
    <[l := p]> T !!nr l = Some p.2.
  Proof. by rewrite /timeNap_lookup_nread lookup_insert. Qed.

  Lemma timeNap_lookup_w_insert_ne T l l' p :
    l ≠ l' → <[l := p]> T !!w l' = T !!w l'.
  Proof. move => ?. by rewrite /timeNap_lookup_write lookup_insert_ne. Qed.

  Lemma timeNap_lookup_ar_insert_ne T l l' p :
    l ≠ l' → <[l := p]> T !!ar l' = T !!ar l'.
  Proof. move => ?. by rewrite /timeNap_lookup_aread lookup_insert_ne. Qed.

  Lemma timeNap_lookup_nr_insert_ne T l l' p :
    l ≠ l' → <[l := p]> T !!nr l' = T !!nr l'.
  Proof. move => ?. by rewrite /timeNap_lookup_nread lookup_insert_ne. Qed.

  Lemma timeNap_lookup_w_singleton_Some l p l' t:
    {[l := p]} !!w l' = Some t → l' = l ∧ t = p.1.1.
  Proof.
    rewrite /timeNap_lookup_write. case (decide (l' = l)) => ?; [subst l'|].
    - by rewrite lookup_insert => [[<-]].
    - by rewrite lookup_insert_ne.
  Qed.

  Lemma timeNap_lookup_ar_singleton_Some l p l' t:
    {[l := p]} !!ar l' = Some t → l' = l ∧ t = p.1.2.
  Proof.
    rewrite /timeNap_lookup_aread. case (decide (l' = l)) => ?; [subst l'|].
    - by rewrite lookup_insert => [[<-]].
    - by rewrite lookup_insert_ne.
  Qed.

  Lemma timeNap_lookup_nr_singleton_Some l p l' t:
    {[l := p]} !!nr l' = Some t → l' = l ∧ t = p.2.
  Proof.
    rewrite /timeNap_lookup_nread. case (decide (l' = l)) => ?; [subst l'|].
    - by rewrite lookup_insert => [[<-]].
    - by rewrite lookup_insert_ne.
  Qed.

  Lemma timeNap_lookup_w_singleton_None l p l':
    {[l := p]} !!w l' = None → l' ≠ l.
  Proof.
    rewrite /timeNap_lookup_write. case (decide (l' = l)) => [?|//]. subst l'.
    by rewrite lookup_insert.
  Qed.

  Lemma timeNap_lookup_ar_singleton_None l p l':
    {[l := p]} !!ar l' = None → l' ≠ l.
  Proof.
    rewrite /timeNap_lookup_aread. case (decide (l' = l)) => [?|//]. subst l'.
    by rewrite lookup_insert.
  Qed.

  Lemma timeNap_lookup_nr_singleton_None l p l':
    {[l := p]} !!nr l' = None → l' ≠ l.
  Proof.
    rewrite /timeNap_lookup_nread. case (decide (l' = l)) => [?|//]. subst l'.
    by rewrite lookup_insert.
  Qed.

End TimeNap.

Notation "m !!w i" := (timeNap_lookup_write i m) (at level 20) : stdpp_scope.
Notation "m !!ar i" := (timeNap_lookup_aread i m) (at level 20) : stdpp_scope.
Notation "m !!nr i" := (timeNap_lookup_nread i m) (at level 20) : stdpp_scope.

Tactic Notation "simplify_nap":= repeat
  match goal with
  | H : ?T !! ?l = ?o |- context P [ ?T !!w ?l ] =>
    let o' := eval cbn in ((fst ∘ fst) <$> o) in
    let g := (context P [o']) in
    cut g; first (rewrite (timeNap_lookup_w' T l o H); exact id)
  | H : ?T !! ?l = ?o |- context P [ ?T !!ar ?l ] =>
    let o' := eval cbn in ((snd ∘ fst) <$> o) in
    let g := (context P [o']) in
    cut g; first (rewrite (timeNap_lookup_ar' T l o H); exact id)
  | H : ?T !! ?l = ?o |- context P [ ?T !!nr ?l ] =>
    let o' := eval cbn in (snd <$> o) in
    let g := (context P [o']) in
    cut g; first (rewrite (timeNap_lookup_nr' T l o H); exact id)
  end.

Section TimeNap_more.
  Context `{LocFacts loc}.
  Lemma timeNap_sqsubseteq (T1 T2 : timeNap) (l : loc) :
    T1 !! l ⊑ T2 !! l ↔
    T1 !!w l ⊑ T2 !!w l ∧ T1 !!ar l ⊑ T2 !!ar l ∧ T1 !!nr l ⊑ T2 !!nr l.
  Proof.
    rewrite {1}/sqsubseteq /lat_sqsubseteq /= /option_sqsubseteq.
    split.
    - destruct (T1 !! l) as [[[??]?]|] eqn:Eq1;
      destruct (T2 !! l) as [[[??]?]|] eqn:Eq2;
      simplify_nap; cbn; try done. move => [[]] //.
    - destruct (T1 !! l) as [[[??]?]|] eqn:Eq1;
      destruct (T2 !! l) as [[[??]?]|] eqn:Eq2;
      simplify_nap; cbn; try done. move => [? []] //. intuition.
  Qed.
End TimeNap_more.
