From stdpp Require Export gmap tactics.
From promising Require Export memory mem_order.

Set Default Proof Using "Type".

Section ThreadView.
  Context `{LocFacts loc} `{CVAL: Countable VAL}.

  Global Instance all_gmap_sqsubseteq_decision (M : gmap loc view) (V:option view) :
    Decision (∀ l, M !! l ⊑ V).
  Proof.
    assert (IFF : (∀ l, M !! l ⊑ V) ↔ (Forall (λ lV', Some lV'.2 ⊑ V) (map_to_list M))).
    { rewrite list.Forall_forall. split.
      - intros ? [l V'] Eq%elem_of_map_to_list. by rewrite /= -Eq.
      - intros HM l. destruct (M!!l) eqn:Eq; [|done].
        apply elem_of_map_to_list in Eq. eapply (HM (_,_)). eauto. }
    destruct (decide (Forall (λ lV', Some lV'.2 ⊑ V) (map_to_list M)));
      [left|right]; by rewrite IFF.
   Qed.

  Global Instance all_gmap_sqsubseteq_decision' (M : gmap loc view) (V:view) :
    Decision (∀ l V', M !! l = Some V' → V ⊑ V').
  Proof.
    assert (IFF : (∀ (l : loc) (V' : view), M !! l = Some V' → V ⊑ V') ↔
                  (Forall (λ lV', V ⊑ lV'.2) (map_to_list M))).
    { rewrite list.Forall_forall. split.
      - intros ? [l V'] Eq%elem_of_map_to_list. eauto.
      - intros HM l V' ?. by eapply (HM (_, _)), elem_of_map_to_list. }
    destruct (decide (Forall (λ lV' : loc * view, V ⊑ lV'.2) (map_to_list M)));
      [left|right]; by rewrite IFF.
  Qed.

  Record threadView : Type :=
    mkTView {
        rel : gmap loc view;  (* The latest release write for each location. *)
        frel: view;           (* The latest SC or REL fence. *)
        cur : view;
        acq : view;

        rel_dom_dec :
          bool_decide (dom (gset loc) rel ⊆ dom (gset loc) cur.(rlx));
        rel_cur_dec : bool_decide (∀ l, rel !! l ⊑ Some cur);
        frel_cur_dec : bool_decide (frel ⊑ cur);
        cur_acq_dec : bool_decide (cur ⊑ acq);
      }.

  Lemma rel_dom 𝓥 : dom (gset loc) 𝓥.(rel) ⊆ dom (gset loc) 𝓥.(cur).(rlx).
  Proof. eapply bool_decide_unpack, rel_dom_dec. Qed.
  Lemma rel_cur 𝓥 l : (𝓥.(rel) !! l) ⊑ Some 𝓥.(cur).
  Proof. revert l. eapply bool_decide_unpack, rel_cur_dec. Qed.
  Lemma rel_cur' 𝓥 l : from_option id ∅ (𝓥.(rel) !! l) ⊑ 𝓥.(cur).
  Proof. pose proof (rel_cur 𝓥 l). by destruct lookup. Qed.
  Lemma frel_cur 𝓥 : 𝓥.(frel) ⊑ 𝓥.(cur).
  Proof. eapply bool_decide_unpack, frel_cur_dec. Qed.
  Lemma cur_acq 𝓥 : 𝓥.(cur) ⊑ 𝓥.(acq).
  Proof. eapply bool_decide_unpack, cur_acq_dec. Qed.

  Lemma threadView_eq 𝓥1 𝓥2 :
    𝓥1.(rel) = 𝓥2.(rel) → 𝓥1.(frel) = 𝓥2.(frel) → 𝓥1.(cur) = 𝓥2.(cur) → 𝓥1.(acq) = 𝓥2.(acq) →
    𝓥1 = 𝓥2.
  Proof. destruct 𝓥1, 𝓥2=>/= ????. subst. f_equal; apply proof_irrel. Qed.

  Program Definition init_tview := mkTView ∅ ∅ ∅ ∅ _ _ _ _.
  Solve Obligations with eapply bool_decide_pack; set_solver.

  Global Instance threadViewInhabited : Inhabited threadView.
  Proof. constructor. exact init_tview. Qed.

  Implicit Type (𝓥: threadView (* U+1D4E5 *)).

  Record tview_le 𝓥1 𝓥2 :=
    mkTViewSqSubsetEq {
      tview_sqsubseteq_rel  : 𝓥1.(rel)  ⊑ 𝓥2.(rel);
      tview_sqsubseteq_frel : 𝓥1.(frel) ⊑ 𝓥2.(frel);
      tview_sqsubseteq_cur  : 𝓥1.(cur)  ⊑ 𝓥2.(cur);
      tview_sqsubseteq_acq  : 𝓥1.(acq)  ⊑ 𝓥2.(acq);
    }.

  Program Definition tview_join :=
    λ 𝓥1 𝓥2, mkTView (𝓥1.(rel) ⊔ 𝓥2.(rel)) (𝓥1.(frel) ⊔ 𝓥2.(frel))
                      (𝓥1.(cur) ⊔ 𝓥2.(cur)) (𝓥1.(acq) ⊔ 𝓥2.(acq)) _ _ _ _.
  Next Obligation.
    intros. apply bool_decide_pack. rewrite !gmap_join_dom_union=>l.
    rewrite !elem_of_union=>-[?|?]; [left|right]; by apply rel_dom.
  Qed.
  Next Obligation.
    intros. apply bool_decide_pack=>l. by rewrite lookup_join !rel_cur.
  Qed.
  Next Obligation. intros. apply bool_decide_pack. by rewrite !frel_cur. Qed.
  Next Obligation. intros. apply bool_decide_pack. by rewrite !cur_acq. Qed.

  Program Definition tview_meet :=
    λ 𝓥1 𝓥2, mkTView (𝓥1.(rel) ⊓ 𝓥2.(rel)) (𝓥1.(frel) ⊓ 𝓥2.(frel))
                      (𝓥1.(cur) ⊓ 𝓥2.(cur)) (𝓥1.(acq) ⊓ 𝓥2.(acq)) _ _ _ _.
  Next Obligation.
    intros. apply bool_decide_pack. rewrite !gmap_meet_dom_intersection=>l.
    rewrite !elem_of_intersection=>-[? ?]; split; by apply rel_dom.
  Qed.
  Next Obligation.
    intros. apply bool_decide_pack=>l. by rewrite lookup_meet !rel_cur.
  Qed.
  Next Obligation. intros. apply bool_decide_pack. by rewrite !frel_cur. Qed.
  Next Obligation. intros. apply bool_decide_pack. by rewrite !cur_acq. Qed.

  Program Canonical Structure tview_Lat :=
    Make_Lat threadView (=) tview_le tview_join tview_meet
             _ _ _ _ _ _ _ _ _ _ _ _ _.
  Next Obligation.
    split; [by split|] => ??? [????] [????]. constructor; by etrans.
  Qed.
  Next Obligation.
    move => [????????] [????????] [????] [????]. apply threadView_eq; by apply: anti_symm.
  Qed.
  Next Obligation. split; solve_lat. Qed.
  Next Obligation. split; solve_lat. Qed.
  Next Obligation. move => ??? [????] [????]. split; solve_lat. Qed.
  Next Obligation. split; solve_lat. Qed.
  Next Obligation. split; solve_lat. Qed.
  Next Obligation. move => ??? [????] [????]. split; solve_lat. Qed.

  Global Instance rel_mono : Proper ((⊑) ==> (⊑)) rel.
  Proof. solve_proper. Qed.
  Global Instance acq_mono : Proper ((⊑) ==> (⊑)) acq.
  Proof. solve_proper. Qed.
  Global Instance frel_mono : Proper ((⊑) ==> (⊑)) frel.
  Proof. solve_proper. Qed.
  Global Instance cur_mono : Proper ((⊑) ==> (⊑)) cur.
  Proof. solve_proper. Qed.

  Global Instance rel_mono_flip : Proper (flip (⊑) ==> flip (⊑)) rel.
  Proof. solve_proper. Qed.
  Global Instance acq_mono_flip : Proper (flip (⊑) ==> flip (⊑)) acq.
  Proof. solve_proper. Qed.
  Global Instance frel_mono_flip : Proper (flip (⊑) ==> flip (⊑)) frel.
  Proof. solve_proper. Qed.
  Global Instance cur_mono_flip : Proper (flip (⊑) ==> flip (⊑)) cur.
  Proof. solve_proper. Qed.

  Global Instance tview_Lat_bot : LatBottom init_tview.
  Proof. done. Qed.

  Notation memory := (memory (loc:=loc) (VAL:=VAL)).
  Implicit Type (M: memory).

  Record closed_tview' 𝓥 (M: memory) :=
    { closed_tview_rel: ∀ l, (𝓥.(rel) !! l) ∈ M;
      closed_tview_frel: 𝓥.(frel) ∈ M;
      closed_tview_cur: 𝓥.(cur) ∈ M;
      closed_tview_acq: 𝓥.(acq) ∈ M; }.

  Global Instance closed_tview : ElemOf threadView memory := closed_tview'.

  Global Instance closed_tview_downclosed :
    Proper ((@sqsubseteq threadView _) ==> (@eq memory) ==> flip impl) (∈).
  Proof.
    move => [????????] [????????] [/= SE1 SE2 SE3 SE4] ?? -> [/=????].
    constructor => /=.
    - move => l. by rewrite (SE1 l).
    - by rewrite SE2.
    - by rewrite SE3.
    - by rewrite SE4.
  Qed.

  Lemma closed_timemap_wf_tview 𝓥 M (HC: 𝓥.(acq).(rlx) ∈ M):
    𝓥 ∈ M.
  Proof.
    have HAcq: 𝓥.(acq) ∈ M.
    { constructor; last done. by rewrite pln_rlx. }
    have HCur: 𝓥.(cur) ∈ M by rewrite cur_acq.
    constructor; [|by rewrite frel_cur|done..].
    by move => l; rewrite rel_cur.
  Qed.

  (* <rel,cur,acq> -{o,l,t,R}-> <cur',acq',rel> *)
  Program Definition read_tview 𝓥 o R V
    (cur' := if decide (AcqRel ⊑ o) then 𝓥.(cur) ⊔ V ⊔ R else 𝓥.(cur) ⊔ V)
    (acq' := if decide (Relaxed ⊑ o) then 𝓥.(acq) ⊔ V ⊔ R else 𝓥.(acq) ⊔ V)
    := (mkTView 𝓥.(rel) 𝓥.(frel) cur' acq' _ _ _ _).
  Next Obligation.
    intros. apply bool_decide_pack. etrans; [apply rel_dom|f_equiv; subst cur'].
    case_match; solve_lat.
  Qed.
  Next Obligation.
    intros. apply bool_decide_pack=>l. rewrite rel_cur /cur'. case_match; solve_lat.
  Qed.
  Next Obligation.
    intros. apply bool_decide_pack. rewrite frel_cur /cur'. case_match; solve_lat.
  Qed.
  Next Obligation.
    intros. apply bool_decide_pack. destruct o; rewrite /cur' /acq' /= cur_acq; solve_lat.
  Qed.
  Inductive read_helper 𝓥 (o: memOrder) l t tr (R: view) : threadView → Prop :=
    | ReadHelper
        (PLN: 𝓥.(cur).(pln) !!w l ⊑ Some t)
        (PLN2: R.(pln) !!w l ⊑ Some t)
        (RLX: Relaxed ⊑ o → 𝓥.(cur).(rlx) !!w l ⊑ Some t)
        (V    := if decide (Relaxed ⊑ o) then mkView_same {[l := (t,{[tr]},∅)]}
                 else mkView_rlx {[l := (t,∅,{[tr]})]})
        (cur' := if decide (AcqRel ⊑ o) then 𝓥.(cur) ⊔ V ⊔ R else 𝓥.(cur) ⊔ V)
        (acq' := if decide (Relaxed ⊑ o) then 𝓥.(acq) ⊔ V ⊔ R else 𝓥.(acq) ⊔ V)
    : read_helper 𝓥 o l t tr R (read_tview 𝓥 o R V).

  (* <rel,cur,acq> -{o,l,t,Rr,Rw}-> <cur',acq',rel'> *)
  Section write.
    Context 𝓥 o l t
            (V    := mkView_same {[l := (t,∅,∅)]})
            (Vra  := if decide (AcqRel ⊑ o) then 𝓥.(cur) ⊔ V else V)
            (V'   := from_option id ∅ (𝓥.(rel) !! l) ⊔ Vra)
            (rel' := <[l := V']> (𝓥.(rel))).

    Program Definition write_tview :=
      mkTView rel' 𝓥.(frel) (𝓥.(cur) ⊔ V) (𝓥.(acq) ⊔ V) _ _ _ _.
    Next Obligation.
      intros. apply bool_decide_pack. pose proof (rel_dom 𝓥).
      rewrite /rel' dom_insert gmap_join_dom_union dom_singleton. set_solver.
    Qed.
    Next Obligation.
      intros. apply bool_decide_pack=>l'. destruct (decide (l = l')) as [<-|].
      - rewrite lookup_insert /V' rel_cur' /Vra. case_match; solve_lat.
      - rewrite lookup_insert_ne // rel_cur. solve_lat.
    Qed.
    Next Obligation. intros. apply bool_decide_pack. rewrite frel_cur. solve_lat. Qed.
    Next Obligation. intros. apply bool_decide_pack. by rewrite cur_acq. Qed.

    Definition write_Rw Rr :=
      if decide (Relaxed ⊑ o) then Some (V' ⊔ 𝓥.(frel) ⊔ Rr) else None.
  End write.
  Inductive write_helper 𝓥 o l t Rr : option view → threadView → Prop :=
    | WriteHelper
      (RLX: 𝓥.(cur).(rlx) !!w l ⊏ Some t)
    : write_helper 𝓥 o l t Rr (write_Rw 𝓥 o l t Rr) (write_tview 𝓥 o l t).

  (* <𝓥,𝓢> -{ F_sc }-> <𝓥',𝓢'> *)
  Program Definition sc_fence_tview 𝓥 𝓢 :=
    let 𝓢' := 𝓥.(acq).(rlx) ⊔ 𝓢 in
    mkTView 𝓥.(rel) (mkView_same 𝓢') (mkView_same 𝓢') (mkView_same 𝓢') _ _ _ _.
  Next Obligation.
    intros. apply bool_decide_pack. etrans; [apply rel_dom|]. f_equiv.
    rewrite cur_acq. solve_lat.
  Qed.
  Next Obligation.
    intros. apply bool_decide_pack=>l. rewrite rel_cur cur_acq.
    split; [rewrite pln_rlx|]; solve_lat.
  Qed.
  Next Obligation. intros. apply bool_decide_pack=>//. Qed.
  Next Obligation. intros. apply bool_decide_pack=>//. Qed.

  Inductive sc_fence_helper 𝓥 𝓢 : threadView → timeNap → Prop :=
    | SCFenceHelper (𝓢' := 𝓥.(acq).(rlx) ⊔ 𝓢 )
    : sc_fence_helper 𝓥 𝓢 (sc_fence_tview 𝓥 𝓢) 𝓢'.

  Inductive alloc_helper : list (@message _ _ VAL) → relation threadView :=
    | AllocListNone 𝓥: alloc_helper nil 𝓥 𝓥
    | AllocListSome 𝑚 𝑚s 𝓥1 𝓥2 𝓥3
        (NEXT: alloc_helper 𝑚s 𝓥1 𝓥2)
        (WRITE: write_helper 𝓥2 Plain 𝑚.(mloc) 𝑚.(mto) ∅ None 𝓥3)
        : alloc_helper (𝑚 :: 𝑚s) 𝓥1 𝓥3.

  Lemma read_helper_tview_sqsubseteq 𝓥 𝓥' o l t tr R
    (READ: read_helper 𝓥 o l t tr R 𝓥'):
    𝓥 ⊑ 𝓥'.
  Proof.
    inversion READ. subst V cur' acq'. constructor=>//=; clear; case_match; solve_lat.
  Qed.

  Lemma write_helper_tview_sqsubseteq 𝓥 𝓥' o l t Rr Rw
    (WRITE: write_helper 𝓥 o l t Rr Rw 𝓥'):
    𝓥 ⊑ 𝓥'.
  Proof.
    inversion_clear WRITE.
    constructor; (try solve_lat) => l'.
    case (decide (l' = l)) => [->|?]; [rewrite lookup_insert|by rewrite lookup_insert_ne].
    case: (rel 𝓥 !! l) => [?|]; solve_lat.
  Qed.

  Lemma read_helper_closed_tview 𝓥 𝓥' o l t tr R M
    (READ: read_helper 𝓥 o l t tr R 𝓥')
    (CLOSED: 𝓥 ∈ M) (CR: R ∈ M) (SOME: ∃ m, M !! (l, t) = Some m):
    𝓥' ∈ M.
  Proof.
    inversion READ. clear H0.
    have ?: {[l := (t,{[tr]},∅)]} ∈ M.
    { move => ??.
      rewrite /timeNap_lookup_write fmap_Some.
      move => [[? ?] []] /lookup_singleton_Some [<- <-] /= ->. naive_solver. }
    have ?: {[l := (t,∅,{[tr]})]} ∈ M.
    { move => ??.
      rewrite /timeNap_lookup_write fmap_Some.
      move => [[? ?] []] /lookup_singleton_Some [<- <-] /= ->. naive_solver. }
    have ?: V ∈ M by subst V; by constructor; case_match.
    have ?: cur 𝓥 ⊔ V ∈ M by apply join_closed_view; [apply CLOSED|by auto].
    have ?: acq 𝓥 ⊔ V ∈ M by apply join_closed_view; [apply CLOSED|by auto].
    subst cur' acq'. constructor; simpl; [apply CLOSED|apply CLOSED|..].
    - case (decide (AcqRel ⊑ _)) => _ /=; [by apply join_closed_view|by auto].
    - subst V. case_match; [by apply join_closed_view|by auto].
  Qed.

  Lemma mem_addins_closed_tview 𝓥 𝓥' o Rr M1 𝑚 M2 k
    (WRITE: write_helper 𝓥 o (mloc 𝑚) (mto 𝑚) Rr 𝑚.(mbase).(mrel) 𝓥')
    (MADD: memory_addins 𝑚 k M1 M2)
    (CLOSED: 𝓥 ∈ M1) (WF: Wf M1) (WFm : Wf 𝑚):
    𝓥' ∈ M2.
  Proof.
    inversion WRITE. clear H0.
    have ?: {[mloc 𝑚 := (mto 𝑚,∅,∅)]} ∈ M2.
    { move => ??.
      rewrite /timeNap_lookup_write fmap_Some.
      move => [[? ?] []] /lookup_singleton_Some [<- <-] /= ->. do 2 eexists.
      split; last by eapply lookup_mem_addins_new. done. }
    have ?: 𝓥.(frel) ∈ M2.
    { eapply closed_view_addins_mono; eauto. by apply CLOSED. }
    constructor; simpl; [|done|..].
    - move => l. case (decide (l = mloc 𝑚)) => [->|?];
        [rewrite lookup_insert|rewrite lookup_insert_ne; last done].
      + repeat apply join_closed_view=>//.
        * pose proof (closed_tview_rel _ _ CLOSED (mloc 𝑚)).
          destruct (rel 𝓥 !! mloc 𝑚) eqn:?; [|done].
          by eapply closed_view_addins_mono.
        * case (decide (AcqRel ⊑ _)) => _ //=.
          apply join_closed_view=>//.
          eapply closed_view_addins_mono; eauto. apply CLOSED.
      + eapply opt_closed_view_addins_mono=>//. apply CLOSED.
    - apply join_closed_view; [|by auto].
      eapply closed_view_addins_mono; eauto. apply CLOSED.
    - apply join_closed_view; [|by auto].
      eapply closed_view_addins_mono; eauto. apply CLOSED.
  Qed.

  Lemma write_helper_closed_tview 𝓥 𝓥' o Rr P1 M1 𝑚 P2 M2 k b
    (WRITE: write_helper 𝓥 o (mloc 𝑚) (mto 𝑚) Rr 𝑚.(mbase).(mrel) 𝓥')
    (MWRITE: memory_write P1 M1 𝑚 P2 M2 k b)
    (CLOSED: 𝓥 ∈ M1) (WF: Wf M1) :
    𝓥' ∈ M2.
  Proof.
    inversion WRITE. clear H0.
    have ?: {[mloc 𝑚 := (mto 𝑚,∅,∅)]} ∈ M2.
    { move => ??.
      rewrite /timeNap_lookup_write fmap_Some.
      move => [[? ?] []] /lookup_singleton_Some [<- <-] /= ->. do 2 eexists.
      split; last by eapply memory_write_new. done. }
    have ?: 𝓥.(frel) ∈ M2.
    { eapply memory_write_closed_view; eauto. by apply CLOSED. }
    constructor; simpl; [|done|..].
    - move => l.
      case (decide (l = mloc 𝑚)) => [->|?];
        [rewrite lookup_insert|rewrite lookup_insert_ne; last done].
      + repeat apply join_closed_view=>//.
        * pose proof (closed_tview_rel _ _ CLOSED (mloc 𝑚)).
          destruct (rel 𝓥 !! mloc 𝑚)=>//.
          eapply memory_write_closed_view=>//.
        * case (decide (AcqRel ⊑ _)) => _ /=; [|done].
          apply join_closed_view; [|done].
          eapply memory_write_closed_view; eauto. apply CLOSED.
      + eapply memory_write_opt_closed_view; eauto. by apply CLOSED.
    - apply join_closed_view; [|by auto].
      eapply memory_write_closed_view; eauto. apply CLOSED.
    - apply join_closed_view; [|by auto].
      eapply memory_write_closed_view; eauto. apply CLOSED.
  Qed.

  Lemma sc_fence_helper_closed_sc 𝓥 𝓥' 𝓢 𝓢' M
    (SC: sc_fence_helper 𝓥 𝓢 𝓥' 𝓢')
    (CLOSED: 𝓥 ∈ M) (CS: 𝓢 ∈ M):
    𝓢' ∈ M.
  Proof. inversion SC. apply join_closed_timenap; [by apply CLOSED|by auto]. Qed.

  Lemma sc_fence_helper_tview_sqsubseteq 𝓥 𝓥' 𝓢 𝓢'
    (SC: sc_fence_helper 𝓥 𝓢 𝓥' 𝓢') :
    𝓥 ⊑ 𝓥'.
  Proof.
    inversion SC.
    have ? : 𝓥.(acq) ⊑ mkView_same (𝓥.(acq).(rlx) ⊔ 𝓢)
      by split; [rewrite pln_rlx|]; solve_lat.
    constructor; rewrite /sc_fence_tview //= ?frel_cur cur_acq //.
  Qed.

  Lemma sc_fence_helper_sc_sqsubseteq 𝓥 𝓥' 𝓢 𝓢'
    (SC: sc_fence_helper 𝓥 𝓢 𝓥' 𝓢') :
    𝓢 ⊑ 𝓢'.
  Proof. inversion SC. solve_lat. Qed.

  Lemma sc_fence_helper_closed_tview 𝓥 𝓥' 𝓢 𝓢' M
    (SC: sc_fence_helper 𝓥 𝓢 𝓥' 𝓢')
    (CLOSED: 𝓥 ∈ M) (CS: 𝓢 ∈ M):
    𝓥' ∈ M.
  Proof.
    inversion SC.
    have ?: 𝓢' ∈ M by eapply sc_fence_helper_closed_sc.
    subst. constructor; simpl; [|by constructor..]. apply CLOSED.
  Qed.

  Lemma alloc_helper_mem_closed_tview
        𝓥1 𝓥2 (𝑚s: list message) M1 M2
    (NONE: ∀ (n' : nat) 𝑚, 𝑚s !! n' = Some 𝑚 → 𝑚.(mbase).(mrel) = None)
    (MALL: mem_list_addins 𝑚s M1 M2)
    (VALL: alloc_helper 𝑚s 𝓥1 𝓥2)
    (CLOSED: 𝓥1 ∈ M1) (WF: Wf M1) :
    𝓥2 ∈ M2.
  Proof.
    revert 𝓥1 𝓥2 M1 M2 CLOSED WF MALL VALL.
    induction 𝑚s; move => 𝓥1 𝓥2 M1 M2 CLOSED WF MALL VALL.
    - inversion VALL. inversion MALL. by subst.
    - inversion VALL. inversion MALL. subst.
      assert (NONE': ∀ (n' : nat) 𝑚, 𝑚s !! n' = Some 𝑚 → mrel (mbase 𝑚) = None).
      { move => n' 𝑚 In. eapply (NONE (n' + 1)).
        rewrite (lookup_app_r (a :: nil)); simpl; last by omega.
        rewrite (_: n' + 1 - 1 = n'); [done|by omega]. }
      eapply mem_addins_closed_tview; eauto.
      + by rewrite (NONE 0 a).
      + eapply wf_mem_list_addins; eauto.
  Qed.

  Lemma alloc_helper_tview_sqsubseteq 𝑚s 𝓥 𝓥'
    (ALLOC: alloc_helper 𝑚s 𝓥 𝓥') :
    𝓥 ⊑ 𝓥'.
  Proof.
    induction ALLOC; first by auto.
    apply write_helper_tview_sqsubseteq in WRITE. etrans; eauto.
  Qed.

  Lemma alloc_helper_cur_sqsubseteq 𝑚s 𝓥1 𝓥2
    (ALLOC: alloc_helper 𝑚s 𝓥1 𝓥2) :
    ∀ 𝑚, 𝑚 ∈ 𝑚s → Some (mto 𝑚) ⊑ pln (cur 𝓥2) !!w mloc 𝑚.
  Proof.
    move : 𝓥2 ALLOC.
    induction 𝑚s as [|𝑚 𝑚s IH] => 𝓥3 ALLOC 𝑚'; first by inversion 1.
    inversion_clear ALLOC.
    move => /elem_of_cons [->|In].
    - inversion WRITE. rewrite timeNap_lookup_w_join timeNap_lookup_w_insert.
      solve_lat.
    - etrans; first apply (IH _ NEXT _ In).
      rewrite /timeNap_lookup_write.
      apply fmap_sqsubseteq; [apply _|].
      eapply write_helper_tview_sqsubseteq, WRITE.
  Qed.

  Lemma alloc_helper_aread_ids  𝑚s 𝓥1 𝓥2
    (ALLOC: alloc_helper 𝑚s 𝓥1 𝓥2) :
    ∀ 𝑚, 𝑚 ∈ 𝑚s → Some ∅ ⊑ 𝓥2.(cur).(rlx) !!ar mloc 𝑚.
  Proof.
    move : 𝓥2 ALLOC.
    induction 𝑚s as [|𝑚 𝑚s IH] => 𝓥3 ALLOC 𝑚'; first by inversion 1.
    inversion_clear ALLOC.
    move => /elem_of_cons [->|In].
    - inversion WRITE. rewrite timeNap_lookup_ar_join timeNap_lookup_ar_insert.
      solve_lat.
    - etrans; first apply (IH _ NEXT _ In).
      rewrite /timeNap_lookup_write.
      apply fmap_sqsubseteq; [apply _|].
      eapply write_helper_tview_sqsubseteq, WRITE.
  Qed.

  Lemma alloc_helper_nread_ids  𝑚s 𝓥1 𝓥2
    (ALLOC: alloc_helper 𝑚s 𝓥1 𝓥2) :
    ∀ 𝑚, 𝑚 ∈ 𝑚s → Some ∅ ⊑ 𝓥2.(cur).(rlx) !!nr mloc 𝑚.
  Proof.
    move : 𝓥2 ALLOC.
    induction 𝑚s as [|𝑚 𝑚s IH] => 𝓥3 ALLOC 𝑚'; first by inversion 1.
    inversion_clear ALLOC.
    move => /elem_of_cons [->|In].
    - inversion WRITE. rewrite timeNap_lookup_nr_join timeNap_lookup_nr_insert.
      solve_lat.
    - etrans; first apply (IH _ NEXT _ In).
      rewrite /timeNap_lookup_write.
      apply fmap_sqsubseteq; [apply _|].
      eapply write_helper_tview_sqsubseteq, WRITE.
  Qed.

  Lemma alloc_helper_cur_old 𝑚s 𝓥1 𝓥2 l
    (UPDATE : alloc_helper 𝑚s 𝓥1 𝓥2) (NONE: ∀ 𝑚, 𝑚 ∈ 𝑚s → l ≠ 𝑚.(mloc)):
    𝓥1.(cur).(rlx) !! l = 𝓥2.(cur).(rlx) !! l.
  Proof.
    induction UPDATE; first done.
    rewrite IHUPDATE.
    - inversion WRITE. rewrite lookup_join lookup_insert_ne; last first.
      { move => ?. eapply NONE; [by left|done]. }
      by rewrite lookup_empty right_id_L.
    - move => 𝑚' ?. apply NONE. by right.
  Qed.

  Lemma alloc_helper_rel_old 𝑚s 𝓥1 𝓥2 l
    (UPDATE : alloc_helper 𝑚s 𝓥1 𝓥2) (NONE: ∀ 𝑚, 𝑚 ∈ 𝑚s → l ≠ 𝑚.(mloc)):
    𝓥1.(rel) !! l = 𝓥2.(rel) !! l.
  Proof.
    induction UPDATE; first done.
    rewrite IHUPDATE.
    - inversion WRITE. rewrite /= lookup_insert_ne //.
      move => ?. eapply NONE; [by left|done].
    - move => 𝑚' ?. apply NONE. by right.
  Qed.

End ThreadView.
