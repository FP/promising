From promising Require Export time location timenap.
Set Default Proof Using "Type".


Section View.
  Context `{LocFacts loc}.

  Definition timeMap := gmap loc time.
  Definition timeMap_Lat := gmap_Lat loc time_Lat.

  Definition timeNap_Lat :=
    gmap_Lat loc (prod_Lat (prod_Lat time_Lat (gset_Lat read_id)) (gset_Lat read_id)).

  Record view :=
    mkView {
        pln : timeNap;
        rlx : timeNap;
        pln_rlx_dec : bool_decide (pln ⊑ rlx)
      }.

  Implicit Types (T: timeNap) (V: view).

  Lemma pln_rlx (V : view) : V.(pln) ⊑ V.(rlx).
  Proof. eapply bool_decide_unpack, pln_rlx_dec. Qed.
  Lemma view_eq V1 V2 : V1.(pln) = V2.(pln) → V1.(rlx) = V2.(rlx) → V1 = V2.
  Proof. destruct V1, V2=>/= ??. subst. f_equal. apply proof_irrel. Qed.

  Program Definition mkView_same (tm : timeNap) := mkView tm tm _.
  Next Obligation. intros. by apply bool_decide_pack. Qed.
  Program Definition mkView_rlx (tm : timeNap) := mkView ∅ tm _.
  Next Obligation. intros. by apply bool_decide_pack. Qed.

  Global Program Instance empty_view : Empty view := mkView ∅ ∅ _.
  Next Obligation. by apply bool_decide_pack. Qed.

  (** View join semilattice *)
  Record view_le V1 V2 :=
    mkViewSqSubsetEq {
      view_sqsubseteq_pln : V1.(pln) ⊑ V2.(pln);
      view_sqsubseteq_rlx : V1.(rlx) ⊑ V2.(rlx);
    }.

  Program Canonical Structure view_Lat :=
    Make_Lat view (=) view_le
      (λ V1 V2, mkView (V1.(pln) ⊔ V2.(pln)) (V1.(rlx) ⊔ V2.(rlx)) _)
      (λ V1 V2, mkView (V1.(pln) ⊓ V2.(pln)) (V1.(rlx) ⊓ V2.(rlx)) _)
      _ _ _ _ _ _ _ _ _ _ _ _ _.
  Next Obligation. move => W1 W2. apply bool_decide_pack, lat_join_mono; apply pln_rlx. Qed.
  Next Obligation. move => W1 W2. apply bool_decide_pack, lat_meet_mono; apply pln_rlx. Qed.
  Next Obligation. split. move=>[]//. move=>??? [??][??]; split; by etrans. Qed.
  Next Obligation. move=>[???][???] [??] [??]. apply view_eq; solve_lat. Qed.
  Next Obligation. move=>[???][???]. split; solve_lat. Qed.
  Next Obligation. move=>[???][???]. split; solve_lat. Qed.
  Next Obligation. move=>[???][???][???] [??][??]. split; solve_lat. Qed.
  Next Obligation. move=>[???][???]. split; solve_lat. Qed.
  Next Obligation. move=>[???][???]. split; solve_lat. Qed.
  Next Obligation. move=>[???][???][???] [??][??]. split; solve_lat. Qed.

  Global Instance rlx_mono : Proper ((⊑) ==> (⊑)) rlx.
  Proof. solve_proper. Qed.
  Global Instance pln_mono : Proper ((⊑) ==> (⊑)) pln.
  Proof. solve_proper. Qed.
  Global Instance rlx_mono_flip : Proper (flip (⊑) ==> flip (⊑)) rlx.
  Proof. solve_proper. Qed.
  Global Instance pln_mono_flip : Proper (flip (⊑) ==> flip (⊑)) pln.
  Proof. solve_proper. Qed.

  Global Instance view_leibniz_equiv : LeibnizEquiv view.
  Proof. move=>?//. Qed.

  Global Instance view_bottom : @LatBottom view_Lat ∅.
  Proof. split; apply lat_bottom_sqsubseteq. Qed.

  Global Instance view_sqsubseteq_decision : RelDecision (@sqsubseteq view _).
  Proof.
    intros x y.
    assert (x ⊑ y ↔ x.(pln) ⊑ y.(pln) ∧ x.(rlx) ⊑ y.(rlx)) as IFF
      by by split=>Hxy; split; apply Hxy.
    destruct (decide (x.(pln) ⊑ y.(pln) ∧ x.(rlx) ⊑ y.(rlx))); [left|right];
      rewrite IFF //.
  Qed.

  (** View countable *)
  Section ViewCountable.
    Definition _tm_pair : Type := timeNap * timeNap.
    Definition _view_to_pair (V : view) : _tm_pair := (pln V, rlx V).
    Definition _pair_to_view (V : _tm_pair) : option view :=
      match V with
      | (Tpln, Trlx) =>
        mkView Tpln Trlx <$>
        (if bool_decide _ as b return option (Is_true b) then Some I else None)
      end.
  End ViewCountable.

  Global Instance view_eq_dec : EqDecision view.
  Proof.
    refine (@inj_eq_dec _ _ _ _view_to_pair _).
    intros [] []; cbv. inversion 1. subst. by apply view_eq.
  Qed.

  Global Instance view_countable : Countable view.
  Proof.
    refine (inj_countable _view_to_pair _pair_to_view _).
    intros [pln rlx DEC]. simpl. generalize (mkView pln rlx).
    destruct (bool_decide (pln ⊑ rlx))=>//=. by destruct DEC.
  Qed.

  Lemma lookup_join_pln V1 V2 l:
    pln (V1 ⊔ V2) !! l = (pln V1 ⊔ pln V2) !! l.
  Proof. by destruct V1, V2. Qed.

  Lemma lookup_join_rlx V1 V2 l:
    rlx (V1 ⊔ V2) !! l = (rlx V1 ⊔ rlx V2) !! l.
  Proof. by destruct V1, V2. Qed.

End View.